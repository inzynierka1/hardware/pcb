EESchema Schematic File Version 4
LIBS:battery_tester-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 8000 5500
NoConn ~ 8000 5400
NoConn ~ 8000 5300
Wire Wire Line
	6900 1250 6900 1350
Connection ~ 6900 1250
Wire Wire Line
	6750 1250 6900 1250
Text GLabel 6750 1250 1    50   Input ~ 0
3.3_ref
Wire Wire Line
	5400 5000 5600 5000
Wire Wire Line
	5500 5200 5600 5200
Connection ~ 5500 5200
Wire Wire Line
	5500 5000 5500 5200
Wire Wire Line
	5600 5000 5600 5200
Connection ~ 5600 5000
Connection ~ 5400 5000
Wire Wire Line
	5500 5000 5500 4850
Wire Wire Line
	5600 4900 5600 5000
Wire Wire Line
	5250 5200 5500 5200
Wire Wire Line
	5600 5200 5600 5400
Connection ~ 5600 5200
Wire Wire Line
	5250 5600 5600 5600
Wire Wire Line
	1850 6400 2050 6400
Connection ~ 1850 6400
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5EC95FC9
P 1850 6400
F 0 "#FLG?" H 1850 6475 50  0001 C CNN
F 1 "PWR_FLAG" H 1850 6573 50  0000 C CNN
F 2 "" H 1850 6400 50  0001 C CNN
F 3 "~" H 1850 6400 50  0001 C CNN
	1    1850 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 6900 1750 6900
Connection ~ 1850 6900
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5EC95FD1
P 1850 6900
F 0 "#FLG?" H 1850 6975 50  0001 C CNN
F 1 "PWR_FLAG" H 1850 7073 50  0000 C CNN
F 2 "" H 1850 6900 50  0001 C CNN
F 3 "~" H 1850 6900 50  0001 C CNN
	1    1850 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6400 2750 6400
Connection ~ 2900 6400
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5EC95FD9
P 2900 6400
F 0 "#FLG?" H 2900 6475 50  0001 C CNN
F 1 "PWR_FLAG" H 2900 6573 50  0000 C CNN
F 2 "" H 2900 6400 50  0001 C CNN
F 3 "~" H 2900 6400 50  0001 C CNN
	1    2900 6400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EC95FDF
P 3000 6400
AR Path="/5D7E29BE/5EC95FDF" Ref="#PWR?"  Part="1" 
AR Path="/5EC95FDF" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC95FDF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3000 6250 50  0001 C CNN
F 1 "+5V" V 3000 6600 50  0000 C CNN
F 2 "" H 3000 6400 50  0001 C CNN
F 3 "" H 3000 6400 50  0001 C CNN
	1    3000 6400
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 6400 2900 6400
Wire Wire Line
	2900 6900 3050 6900
Connection ~ 2900 6900
Wire Wire Line
	2750 6900 2900 6900
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5EC95FE9
P 2900 6900
F 0 "#FLG?" H 2900 6975 50  0001 C CNN
F 1 "PWR_FLAG" H 2900 7073 50  0000 C CNN
F 2 "" H 2900 6900 50  0001 C CNN
F 3 "~" H 2900 6900 50  0001 C CNN
	1    2900 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3550 6900 3650
Wire Wire Line
	6900 3550 6850 3550
$Comp
L power:GND #PWR?
U 1 1 5EC95FF1
P 6900 3650
AR Path="/5D7E29BE/5EC95FF1" Ref="#PWR?"  Part="1" 
AR Path="/5EC95FF1" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC95FF1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6900 3400 50  0001 C CNN
F 1 "GND" H 6900 3500 50  0000 C CNN
F 2 "" H 6900 3650 50  0001 C CNN
F 3 "" H 6900 3650 50  0001 C CNN
	1    6900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5EC95FF7
P 6700 3550
AR Path="/5D7E29BE/5EC95FF7" Ref="D?"  Part="1" 
AR Path="/5EC95FF7" Ref="D?"  Part="1" 
AR Path="/5EC577BA/5EC95FF7" Ref="D?"  Part="1" 
F 0 "D?" H 6850 3650 50  0000 R CNN
F 1 "LED" H 6900 3500 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric" H 6700 3550 50  0001 C CNN
F 3 "~" H 6700 3550 50  0001 C CNN
	1    6700 3550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EC95FFD
P 6350 3550
AR Path="/5D7E29BE/5EC95FFD" Ref="R?"  Part="1" 
AR Path="/5EC95FFD" Ref="R?"  Part="1" 
AR Path="/5EC577BA/5EC95FFD" Ref="R?"  Part="1" 
F 0 "R?" V 6450 3550 50  0000 C CNN
F 1 "680" V 6350 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6280 3550 50  0001 C CNN
F 3 "~" H 6350 3550 50  0001 C CNN
	1    6350 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	6550 3550 6500 3550
Wire Wire Line
	6200 3950 7100 3950
Text GLabel 8000 4050 2    50   Input ~ 0
vin_load_sens
Text GLabel 8000 5200 2    50   Output ~ 0
vout_charge_sens
Text GLabel 8000 5500 2    50   Output ~ 0
charge_on_off
Text GLabel 8000 5300 2    50   Output ~ 0
cv_pwm
Text GLabel 8000 5400 2    50   Output ~ 0
cc_pwm
Text GLabel 8000 5600 2    50   Output ~ 0
cc_voltage_sense
Connection ~ 7200 5200
$Comp
L power:GND #PWR?
U 1 1 5EC9600C
P 7850 3600
AR Path="/5D7E29BE/5EC9600C" Ref="#PWR?"  Part="1" 
AR Path="/5EC9600C" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9600C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7850 3350 50  0001 C CNN
F 1 "GND" H 7850 3450 50  0000 C CNN
F 2 "" H 7850 3600 50  0001 C CNN
F 3 "" H 7850 3600 50  0001 C CNN
	1    7850 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3150 8350 3150
Wire Wire Line
	8350 3150 8550 3150
Wire Wire Line
	8550 3150 8850 3150
Connection ~ 8550 3150
Connection ~ 8350 3150
Connection ~ 7300 3350
Wire Wire Line
	7450 3350 7300 3350
Wire Wire Line
	7450 3500 7450 3350
$Comp
L Device:C_Small C?
U 1 1 5EC9601A
P 7450 3600
AR Path="/5D7E29BE/5EC9601A" Ref="C?"  Part="1" 
AR Path="/5EC9601A" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC9601A" Ref="C?"  Part="1" 
F 0 "C?" V 7350 3600 50  0000 C CNN
F 1 "100nF" V 7550 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7450 3600 50  0001 C CNN
F 3 "~" H 7450 3600 50  0001 C CNN
	1    7450 3600
	1    0    0    1   
$EndComp
Wire Wire Line
	7450 3750 7450 3700
$Comp
L power:GND #PWR?
U 1 1 5EC96021
P 7450 3750
AR Path="/5D7E29BE/5EC96021" Ref="#PWR?"  Part="1" 
AR Path="/5EC96021" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC96021" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7450 3500 50  0001 C CNN
F 1 "GND" H 7450 3600 50  0000 C CNN
F 2 "" H 7450 3750 50  0001 C CNN
F 3 "" H 7450 3750 50  0001 C CNN
	1    7450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5200 7200 5450
Wire Wire Line
	7100 3950 7100 5100
Wire Wire Line
	7700 4050 8000 4050
Wire Wire Line
	6200 3450 7200 3450
Wire Wire Line
	7200 3450 7200 5200
Wire Wire Line
	7850 3250 7850 3350
Wire Wire Line
	7700 3250 7850 3250
Connection ~ 7700 3250
Wire Wire Line
	6200 3250 7700 3250
$Comp
L Device:C_Small C?
U 1 1 5EC96030
P 7850 3450
AR Path="/5D7E29BE/5EC96030" Ref="C?"  Part="1" 
AR Path="/5EC96030" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC96030" Ref="C?"  Part="1" 
F 0 "C?" V 7750 3450 50  0000 C CNN
F 1 "100nF" V 7950 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7850 3450 50  0001 C CNN
F 3 "~" H 7850 3450 50  0001 C CNN
	1    7850 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	7850 3600 7850 3550
Wire Wire Line
	7200 5200 8000 5200
Wire Wire Line
	7200 5650 7200 5800
$Comp
L Device:C_Small C?
U 1 1 5EC96039
P 7200 5550
AR Path="/5D7E2920/5EC96039" Ref="C?"  Part="1" 
AR Path="/5DAD577F/5EC96039" Ref="C?"  Part="1" 
AR Path="/5EC96039" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC96039" Ref="C?"  Part="1" 
F 0 "C?" H 7350 5550 50  0000 C CNN
F 1 "100nF" H 7350 5450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7200 5550 50  0001 C CNN
F 3 "~" H 7200 5550 50  0001 C CNN
	1    7200 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC9603F
P 7200 5800
AR Path="/5D7E2920/5EC9603F" Ref="#PWR?"  Part="1" 
AR Path="/5DAD577F/5EC9603F" Ref="#PWR?"  Part="1" 
AR Path="/5EC9603F" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9603F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7200 5550 50  0001 C CNN
F 1 "GND" H 7205 5627 50  0000 C CNN
F 2 "" H 7200 5800 50  0001 C CNN
F 3 "" H 7200 5800 50  0001 C CNN
	1    7200 5800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 3950 1950 3950
Wire Wire Line
	2250 3950 2250 3400
Text GLabel 1950 3950 0    50   Input ~ 0
bat_minus
Connection ~ 2700 3450
Wire Wire Line
	2700 3350 2800 3350
Wire Wire Line
	2700 3450 2700 3350
Text GLabel 2800 3350 2    50   Input ~ 0
LOAD+
Connection ~ 2700 3550
Wire Wire Line
	2700 3450 2700 3550
Wire Wire Line
	2800 3450 2700 3450
Text GLabel 2800 3450 2    50   Input ~ 0
bat_plus
Connection ~ 2350 3550
Wire Wire Line
	2350 3600 2350 3550
Wire Wire Line
	2350 3950 2700 3950
Connection ~ 2350 3950
Wire Wire Line
	2350 3900 2350 3950
Wire Wire Line
	2250 3950 2350 3950
Wire Wire Line
	2700 3600 2700 3550
Wire Wire Line
	2700 3950 2700 3900
Wire Wire Line
	2350 3400 2350 3550
Connection ~ 2250 3950
Wire Wire Line
	2250 3950 2250 4000
Wire Wire Line
	2350 3550 2700 3550
$Comp
L power:GND #PWR?
U 1 1 5EC9605C
P 2250 4000
AR Path="/5D7E29BE/5EC9605C" Ref="#PWR?"  Part="1" 
AR Path="/5EC9605C" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9605C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2250 3750 50  0001 C CNN
F 1 "GND" H 2255 3827 50  0000 C CNN
F 2 "" H 2250 4000 50  0001 C CNN
F 3 "" H 2250 4000 50  0001 C CNN
	1    2250 4000
	-1   0    0    -1  
$EndComp
$Comp
L Diode:1.5KExxA D?
U 1 1 5EC96062
P 2700 3750
AR Path="/5D7E29BE/5EC96062" Ref="D?"  Part="1" 
AR Path="/5EC96062" Ref="D?"  Part="1" 
AR Path="/5EC577BA/5EC96062" Ref="D?"  Part="1" 
F 0 "D?" H 2650 3650 50  0000 L CNN
F 1 "SMCJ22A" H 2550 3550 50  0000 L CNN
F 2 "Diode_SMD:D_SMC_Handsoldering" H 2700 3550 50  0001 C CNN
F 3 "https://www.vishay.com/docs/88301/15ke.pdf" H 2650 3750 50  0001 C CNN
	1    2700 3750
	0    -1   1    0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5EC96068
P 2350 3750
AR Path="/5D7E29BE/5EC96068" Ref="D?"  Part="1" 
AR Path="/5EC96068" Ref="D?"  Part="1" 
AR Path="/5EC577BA/5EC96068" Ref="D?"  Part="1" 
F 0 "D?" H 2300 3650 50  0000 L CNN
F 1 "MLV0152" H 2200 3550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2350 3750 50  0001 C CNN
F 3 "~" H 2350 3750 50  0001 C CNN
	1    2350 3750
	0    -1   1    0   
$EndComp
Wire Notes Line
	2600 3400 2000 3400
Wire Notes Line
	2000 2950 2600 2950
Wire Notes Line
	2600 2950 2600 3400
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EC96071
P 2350 3200
AR Path="/5D7E29BE/5EC96071" Ref="J?"  Part="1" 
AR Path="/5EC96071" Ref="J?"  Part="1" 
AR Path="/5EC577BA/5EC96071" Ref="J?"  Part="1" 
F 0 "J?" V 2550 3200 50  0000 R CNN
F 1 "battery_load" V 2450 3400 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P7.62mm_Drill2.5mm" H 2350 3200 50  0001 C CNN
F 3 "~" H 2350 3200 50  0001 C CNN
	1    2350 3200
	0    1    -1   0   
$EndComp
Wire Notes Line
	2000 3400 2000 2950
Wire Wire Line
	6300 4550 6900 4550
Wire Wire Line
	6900 4350 6900 4550
Wire Wire Line
	8000 4350 6900 4350
Wire Wire Line
	7100 5100 8000 5100
Wire Wire Line
	6900 4250 8000 4250
Wire Wire Line
	6900 4050 6200 4050
Wire Wire Line
	6900 4250 6900 4050
Wire Wire Line
	7700 3250 7700 4050
Wire Wire Line
	7300 4150 8000 4150
Wire Wire Line
	6200 3350 7300 3350
Wire Wire Line
	7300 3350 7300 4150
$Comp
L power:+5V #PWR?
U 1 1 5EC96083
P 8450 950
AR Path="/5D7E29BE/5EC96083" Ref="#PWR?"  Part="1" 
AR Path="/5EC96083" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC96083" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8450 800 50  0001 C CNN
F 1 "+5V" H 8450 1150 50  0000 C CNN
F 2 "" H 8450 950 50  0001 C CNN
F 3 "" H 8450 950 50  0001 C CNN
	1    8450 950 
	1    0    0    -1  
$EndComp
Wire Notes Line width 8
	8550 700  8550 2200
Wire Wire Line
	5800 1350 5800 1550
Wire Wire Line
	5800 1350 6900 1350
Connection ~ 6900 1350
Wire Wire Line
	6900 1350 6900 1650
Wire Wire Line
	6900 1350 7100 1350
Wire Wire Line
	6900 1200 6900 1250
Wire Wire Line
	5700 4850 5700 4900
Connection ~ 5600 4900
Wire Wire Line
	5600 4850 5600 4900
Wire Wire Line
	5400 4850 5400 5000
Wire Wire Line
	4800 5200 4800 5150
Wire Wire Line
	4950 5200 4800 5200
$Comp
L power:+3.3V #PWR?
U 1 1 5EC96096
P 4800 5150
AR Path="/5D7E29BE/5EC96096" Ref="#PWR?"  Part="1" 
AR Path="/5EC96096" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC96096" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 5000 50  0001 C CNN
F 1 "+3.3V" H 4800 5300 50  0000 C CNN
F 2 "" H 4800 5150 50  0001 C CNN
F 3 "" H 4800 5150 50  0001 C CNN
	1    4800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 7000 3800 7000
$Comp
L power:GND #PWR?
U 1 1 5EC9609D
P 4150 7000
AR Path="/5D7E29BE/5EC9609D" Ref="#PWR?"  Part="1" 
AR Path="/5EC9609D" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9609D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4150 6750 50  0001 C CNN
F 1 "GND" H 4250 7000 50  0000 C CNN
F 2 "" H 4150 7000 50  0001 C CNN
F 3 "" H 4150 7000 50  0001 C CNN
	1    4150 7000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5EC960A3
P 3600 7000
AR Path="/5D7E29BE/5EC960A3" Ref="J?"  Part="1" 
AR Path="/5EC960A3" Ref="J?"  Part="1" 
AR Path="/5EC577BA/5EC960A3" Ref="J?"  Part="1" 
F 0 "J?" H 3708 7281 50  0000 C CNN
F 1 "prog" H 3708 7190 50  0000 C CNN
F 2 "Connector_JST:JST_EH_S4B-EH_1x04_P2.50mm_Horizontal" H 3600 7000 50  0001 C CNN
F 3 "~" H 3600 7000 50  0001 C CNN
	1    3600 7000
	-1   0    0    -1  
$EndComp
Text GLabel 3800 7200 2    50   Input ~ 0
NRST
Text GLabel 3800 7100 2    50   Input ~ 0
SWDIO
Text GLabel 3800 6900 2    50   Input ~ 0
SWCLK
Text GLabel 8000 4350 2    50   Output ~ 0
dac_out
Wire Wire Line
	8350 3250 8350 3150
Wire Wire Line
	5700 4900 5600 4900
Connection ~ 5600 5400
$Comp
L Device:C_Small C?
U 1 1 5EC960B0
P 5150 5000
AR Path="/5D7E29BE/5EC960B0" Ref="C?"  Part="1" 
AR Path="/5EC960B0" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC960B0" Ref="C?"  Part="1" 
F 0 "C?" V 5100 5100 50  0000 C CNN
F 1 "100nF" V 5200 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 5000 50  0001 C CNN
F 3 "~" H 5150 5000 50  0001 C CNN
	1    5150 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 5000 5250 5000
Wire Wire Line
	4950 5000 5050 5000
Wire Wire Line
	4950 5000 4950 5200
Connection ~ 4950 5200
Wire Wire Line
	4950 5200 4950 5400
Connection ~ 4950 5400
Wire Wire Line
	4950 5400 5050 5400
Wire Wire Line
	4950 5400 4950 5600
Wire Wire Line
	5600 5600 5600 5400
Connection ~ 5600 5600
Wire Wire Line
	5600 5400 5250 5400
Wire Wire Line
	5600 5700 5600 5600
Wire Wire Line
	4950 5600 5050 5600
Wire Wire Line
	5050 5200 4950 5200
$Comp
L Device:C_Small C?
U 1 1 5EC960C4
P 5150 5200
AR Path="/5D7E29BE/5EC960C4" Ref="C?"  Part="1" 
AR Path="/5EC960C4" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC960C4" Ref="C?"  Part="1" 
F 0 "C?" V 5100 5300 50  0000 C CNN
F 1 "100nF" V 5200 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 5200 50  0001 C CNN
F 3 "~" H 5150 5200 50  0001 C CNN
	1    5150 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EC960CA
P 5150 5400
AR Path="/5D7E29BE/5EC960CA" Ref="C?"  Part="1" 
AR Path="/5EC960CA" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC960CA" Ref="C?"  Part="1" 
F 0 "C?" V 5100 5500 50  0000 C CNN
F 1 "100nF" V 5200 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 5400 50  0001 C CNN
F 3 "~" H 5150 5400 50  0001 C CNN
	1    5150 5400
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EC960D0
P 5150 5600
AR Path="/5DF6D00C/5EC960D0" Ref="C?"  Part="1" 
AR Path="/5EC960D0" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC960D0" Ref="C?"  Part="1" 
F 0 "C?" V 5200 5500 50  0000 C CNN
F 1 "1uF" V 5100 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5150 5600 50  0001 C CNN
F 3 "~" H 5150 5600 50  0001 C CNN
	1    5150 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC960D6
P 5600 5700
AR Path="/5D7E29BE/5EC960D6" Ref="#PWR?"  Part="1" 
AR Path="/5EC960D6" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC960D6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 5450 50  0001 C CNN
F 1 "GND" H 5700 5700 50  0000 C CNN
F 2 "" H 5600 5700 50  0001 C CNN
F 3 "" H 5600 5700 50  0001 C CNN
	1    5600 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1800 5600 1800
Connection ~ 5600 1800
Wire Wire Line
	5600 1800 5500 1800
Connection ~ 5500 1800
Wire Wire Line
	5500 1800 5500 1850
Wire Wire Line
	5500 1450 5500 1800
$Comp
L power:+3.3V #PWR?
U 1 1 5EC960E2
P 5500 1450
AR Path="/5D7E29BE/5EC960E2" Ref="#PWR?"  Part="1" 
AR Path="/5EC960E2" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC960E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5500 1300 50  0001 C CNN
F 1 "+3.3V" H 5500 1600 50  0000 C CNN
F 2 "" H 5500 1450 50  0001 C CNN
F 3 "" H 5500 1450 50  0001 C CNN
	1    5500 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1850 5600 1800
Wire Wire Line
	5700 1850 5700 1800
Wire Wire Line
	8550 3600 8550 3650
Connection ~ 8550 3600
Wire Wire Line
	8350 3600 8550 3600
Wire Wire Line
	8350 3450 8350 3600
Wire Wire Line
	8550 3450 8550 3600
Connection ~ 6300 4550
Wire Wire Line
	6300 4650 6300 4550
Wire Wire Line
	2050 6600 2050 6700
Connection ~ 2050 6600
Wire Wire Line
	2150 6600 2050 6600
Wire Wire Line
	2050 6500 2050 6600
Connection ~ 2050 6500
Wire Wire Line
	2150 6500 2050 6500
Wire Wire Line
	2150 6400 2050 6400
Wire Wire Line
	2050 7100 2050 7000
Connection ~ 2050 7100
Wire Wire Line
	2150 7100 2050 7100
Wire Wire Line
	2050 7000 2050 6900
Connection ~ 2050 7000
Wire Wire Line
	2150 7000 2050 7000
Wire Wire Line
	2050 6900 2150 6900
Wire Wire Line
	2750 6900 2750 7000
Connection ~ 2750 6900
Wire Wire Line
	2650 6900 2750 6900
Wire Wire Line
	2750 7100 2750 7200
Connection ~ 2750 7100
Wire Wire Line
	2650 7100 2750 7100
Wire Wire Line
	2750 6600 2750 6700
Connection ~ 2750 6600
Wire Wire Line
	2650 6600 2750 6600
Wire Wire Line
	2750 6500 2750 6600
Connection ~ 2750 6500
Wire Wire Line
	2650 6500 2750 6500
Wire Wire Line
	2650 6400 2750 6400
Wire Wire Line
	2750 6400 2750 6500
Connection ~ 2750 6400
Wire Wire Line
	2050 6100 2050 6000
Connection ~ 2050 6100
Wire Wire Line
	2150 6100 2050 6100
Wire Wire Line
	2050 6000 2050 5900
Connection ~ 2050 6000
Wire Wire Line
	2150 6000 2050 6000
Wire Wire Line
	2050 5900 2050 5800
Connection ~ 2050 5900
Wire Wire Line
	2150 5900 2050 5900
Wire Wire Line
	2050 5800 2050 5700
Connection ~ 2050 5800
Wire Wire Line
	2150 5800 2050 5800
Wire Wire Line
	2050 5700 2050 5600
Connection ~ 2050 5700
Wire Wire Line
	2150 5700 2050 5700
Wire Wire Line
	2150 5600 2050 5600
Wire Wire Line
	2750 6000 2650 6000
Wire Wire Line
	2750 6100 2750 6200
Connection ~ 2750 6100
Wire Wire Line
	2650 6100 2750 6100
Wire Wire Line
	2750 5700 2750 5800
Connection ~ 2750 5700
Wire Wire Line
	2650 5700 2750 5700
Wire Wire Line
	2650 5600 2750 5600
Connection ~ 2050 6400
Wire Wire Line
	2050 6300 2050 6400
Wire Wire Line
	2150 6300 2050 6300
Connection ~ 2050 6900
Wire Wire Line
	2050 7200 2150 7200
Wire Wire Line
	2050 7200 2050 7100
Wire Wire Line
	2050 5600 2050 5500
Connection ~ 2050 5600
Wire Wire Line
	1850 5600 2050 5600
Wire Wire Line
	2050 5500 2150 5500
Wire Wire Line
	2050 6200 2050 6100
Wire Wire Line
	2150 6200 2050 6200
Wire Wire Line
	2750 7000 2750 7100
Connection ~ 2750 7000
Wire Wire Line
	2750 7200 2650 7200
Wire Wire Line
	2750 6800 2750 6900
Wire Wire Line
	2650 6800 2750 6800
Wire Wire Line
	2750 6700 2650 6700
Wire Wire Line
	2750 6300 2750 6400
Wire Wire Line
	2050 6900 1850 6900
Wire Wire Line
	2050 6800 2050 6900
Wire Wire Line
	2150 6800 2050 6800
Wire Wire Line
	2050 6700 2150 6700
Wire Wire Line
	2050 6400 2050 6500
Wire Wire Line
	1750 6400 1850 6400
$Comp
L power:GND #PWR?
U 1 1 5EC9613F
P 1750 6900
AR Path="/5D7E29BE/5EC9613F" Ref="#PWR?"  Part="1" 
AR Path="/5EC9613F" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9613F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1750 6650 50  0001 C CNN
F 1 "GND" V 1750 6650 50  0000 C CNN
F 2 "" H 1750 6900 50  0001 C CNN
F 3 "" H 1750 6900 50  0001 C CNN
	1    1750 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 7000 2650 7000
Wire Wire Line
	2650 6300 2750 6300
Wire Wire Line
	2750 6000 2750 6100
Connection ~ 2750 6000
Wire Wire Line
	2900 6000 2750 6000
Wire Wire Line
	2750 6200 2650 6200
Wire Wire Line
	2750 5900 2750 6000
Wire Wire Line
	2650 5900 2750 5900
Wire Wire Line
	2750 5600 2750 5700
Connection ~ 2750 5600
Wire Wire Line
	2750 5600 2900 5600
Wire Wire Line
	2750 5800 2650 5800
Wire Wire Line
	2750 5500 2750 5600
Wire Wire Line
	2650 5500 2750 5500
Connection ~ 6200 1800
Wire Wire Line
	6200 1550 6200 1800
Wire Wire Line
	5800 1550 5800 1800
Connection ~ 5800 1550
Wire Wire Line
	5800 1550 5900 1550
Wire Wire Line
	6200 1550 6100 1550
$Comp
L Device:C_Small C?
U 1 1 5EC96159
P 6000 1550
AR Path="/5DF6D00C/5EC96159" Ref="C?"  Part="1" 
AR Path="/5EC96159" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC96159" Ref="C?"  Part="1" 
F 0 "C?" H 6100 1600 50  0000 C CNN
F 1 "1uF" H 6100 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6000 1550 50  0001 C CNN
F 3 "~" H 6000 1550 50  0001 C CNN
	1    6000 1550
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5EC9615F
P 3050 6900
F 0 "#PWR?" H 3050 6750 50  0001 C CNN
F 1 "+3.3V" V 3065 7028 50  0000 L CNN
F 2 "" H 3050 6900 50  0001 C CNN
F 3 "" H 3050 6900 50  0001 C CNN
	1    3050 6900
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x18_Row_Letter_First J?
U 1 1 5EC96165
P 2350 6300
F 0 "J?" H 2400 7317 50  0000 C CNN
F 1 "Conn_02x18_Row_Letter_First" H 2400 7226 50  0000 C CNN
F 2 "Connector_PCBEdge:BUS_PCIexpress" H 2350 6300 50  0001 C CNN
F 3 "~" H 2350 6300 50  0001 C CNN
	1    2350 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC9616B
P 1850 5600
AR Path="/5D7E29BE/5EC9616B" Ref="#PWR?"  Part="1" 
AR Path="/5EC9616B" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC9616B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1850 5350 50  0001 C CNN
F 1 "GND" V 1850 5350 50  0000 C CNN
F 2 "" H 1850 5600 50  0001 C CNN
F 3 "" H 1850 5600 50  0001 C CNN
	1    1850 5600
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5EC96171
P 1750 6400
AR Path="/5D7E29BE/5EC96171" Ref="#PWR?"  Part="1" 
AR Path="/5EC96171" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC96171" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1750 6500 50  0001 C CNN
F 1 "-5V" V 1765 6528 50  0000 L CNN
F 2 "" H 1750 6400 50  0001 C CNN
F 3 "" H 1750 6400 50  0001 C CNN
	1    1750 6400
	0    -1   -1   0   
$EndComp
Text GLabel 2900 6000 2    50   Input ~ 0
I2C2_SDA
Text GLabel 2900 5600 2    50   Input ~ 0
I2C2_SCL
Text GLabel 8000 5100 2    50   Output ~ 0
mosfet_charge_sw
Text GLabel 8000 4250 2    50   Output ~ 0
mosfet_load_sw
NoConn ~ 5400 1850
NoConn ~ 6200 3650
NoConn ~ 6200 3750
NoConn ~ 6200 3850
NoConn ~ 6200 4150
NoConn ~ 6200 4250
NoConn ~ 6200 4350
NoConn ~ 6200 4650
NoConn ~ 4900 4650
NoConn ~ 4900 4550
NoConn ~ 4900 4450
NoConn ~ 4900 4350
NoConn ~ 4900 4250
NoConn ~ 4900 4150
NoConn ~ 4900 4050
NoConn ~ 4900 3950
NoConn ~ 4900 3850
NoConn ~ 4900 3750
$Comp
L MCU_ST_STM32F3:STM32F301C8Tx U?
U 1 1 5EC9618D
P 5600 3350
AR Path="/5D7E29BE/5EC9618D" Ref="U?"  Part="1" 
AR Path="/5EC9618D" Ref="U?"  Part="1" 
AR Path="/5EC577BA/5EC9618D" Ref="U?"  Part="1" 
F 0 "U?" H 5550 2600 50  0000 C CNN
F 1 "STM32F301C8Tx" H 5550 2700 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5000 1950 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00093332.pdf" H 5600 3350 50  0001 C CNN
	1    5600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4550 6300 4550
NoConn ~ 4900 3650
NoConn ~ 4900 3550
NoConn ~ 4900 3450
NoConn ~ 4900 3350
NoConn ~ 4900 3250
NoConn ~ 4900 3150
NoConn ~ 4900 2950
NoConn ~ 4900 2850
NoConn ~ 4900 2750
Wire Wire Line
	6300 4650 6500 4650
Wire Wire Line
	6500 4450 6200 4450
Text GLabel 4900 2550 0    50   Input ~ 0
I2C2_SCL
Connection ~ 4800 2050
Wire Wire Line
	4000 2050 4800 2050
Wire Wire Line
	4450 2300 4550 2300
Text GLabel 4900 2450 0    50   Input ~ 0
I2C2_SDA
Wire Wire Line
	4800 2050 4900 2050
Wire Wire Line
	4800 1950 4800 2050
Wire Wire Line
	5800 1800 5800 1850
Connection ~ 5800 1800
Wire Wire Line
	5900 1800 5800 1800
Wire Wire Line
	6200 1800 6100 1800
Wire Wire Line
	6200 1950 6200 1800
$Comp
L power:GND #PWR?
U 1 1 5EC961AB
P 6200 1950
AR Path="/5D7E29BE/5EC961AB" Ref="#PWR?"  Part="1" 
AR Path="/5EC961AB" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC961AB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6200 1700 50  0001 C CNN
F 1 "GND" H 6300 1950 50  0000 C CNN
F 2 "" H 6200 1950 50  0001 C CNN
F 3 "" H 6200 1950 50  0001 C CNN
	1    6200 1950
	1    0    0    -1  
$EndComp
Text GLabel 4800 1950 0    50   Input ~ 0
NRST
Text GLabel 6500 4450 2    50   Output ~ 0
SWDIO
Text GLabel 6500 4650 2    50   Output ~ 0
SWCLK
Wire Wire Line
	4850 2250 4900 2250
Wire Wire Line
	4850 2300 4850 2250
$Comp
L Device:R R?
U 1 1 5EC961B6
P 4700 2300
AR Path="/5D7E29BE/5EC961B6" Ref="R?"  Part="1" 
AR Path="/5EC961B6" Ref="R?"  Part="1" 
AR Path="/5EC577BA/5EC961B6" Ref="R?"  Part="1" 
F 0 "R?" V 4600 2300 50  0000 C CNN
F 1 "10k" V 4700 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 2300 50  0001 C CNN
F 3 "~" H 4700 2300 50  0001 C CNN
	1    4700 2300
	0    -1   1    0   
$EndComp
Wire Wire Line
	3800 2600 4000 2600
Connection ~ 4300 2600
Wire Wire Line
	4300 2650 4300 2600
Connection ~ 4300 1650
Wire Wire Line
	4300 1550 4300 1650
Connection ~ 4000 2600
Wire Wire Line
	4300 2600 4000 2600
Wire Wire Line
	4300 2500 4300 2600
Wire Wire Line
	4300 1650 4300 2100
Wire Wire Line
	4300 1650 4000 1650
$Comp
L Jumper:SolderJumper_3_Open JP?
U 1 1 5EC961C6
P 4300 2300
AR Path="/5D7E29BE/5EC961C6" Ref="JP?"  Part="1" 
AR Path="/5EC961C6" Ref="JP?"  Part="1" 
AR Path="/5EC577BA/5EC961C6" Ref="JP?"  Part="1" 
F 0 "JP?" H 4100 2350 50  0000 L CNN
F 1 "boot" H 4050 2200 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 4300 2300 50  0001 C CNN
F 3 "~" H 4300 2300 50  0001 C CNN
	1    4300 2300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC961CC
P 4300 2650
AR Path="/5D7E29BE/5EC961CC" Ref="#PWR?"  Part="1" 
AR Path="/5EC961CC" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC961CC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4300 2400 50  0001 C CNN
F 1 "GND" H 4200 2650 50  0000 C CNN
F 2 "" H 4300 2650 50  0001 C CNN
F 3 "" H 4300 2650 50  0001 C CNN
	1    4300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2600 4000 2400
Wire Wire Line
	3800 2500 3800 2600
Wire Wire Line
	4000 2050 4000 2200
Connection ~ 4000 2050
Wire Wire Line
	4000 1950 4000 2050
Wire Wire Line
	3800 2050 4000 2050
Wire Wire Line
	3800 2050 3800 2100
$Comp
L power:+3.3V #PWR?
U 1 1 5EC961D9
P 4300 1550
AR Path="/5D7E29BE/5EC961D9" Ref="#PWR?"  Part="1" 
AR Path="/5EC961D9" Ref="#PWR?"  Part="1" 
AR Path="/5EC577BA/5EC961D9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4300 1400 50  0001 C CNN
F 1 "+3.3V" H 4300 1700 50  0000 C CNN
F 2 "" H 4300 1550 50  0001 C CNN
F 3 "" H 4300 1550 50  0001 C CNN
	1    4300 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EC961DF
P 4000 1800
AR Path="/5D7E29BE/5EC961DF" Ref="R?"  Part="1" 
AR Path="/5EC961DF" Ref="R?"  Part="1" 
AR Path="/5EC577BA/5EC961DF" Ref="R?"  Part="1" 
F 0 "R?" V 3900 1800 50  0000 C CNN
F 1 "10k" V 4000 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3930 1800 50  0001 C CNN
F 3 "~" H 4000 1800 50  0001 C CNN
	1    4000 1800
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EC961E5
P 4000 2300
AR Path="/5D7E29BE/5EC961E5" Ref="C?"  Part="1" 
AR Path="/5EC961E5" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC961E5" Ref="C?"  Part="1" 
F 0 "C?" V 3950 2200 50  0000 C CNN
F 1 "100nF" V 4100 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4000 2300 50  0001 C CNN
F 3 "~" H 4000 2300 50  0001 C CNN
	1    4000 2300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5EC961EB
P 3800 2300
AR Path="/5D7E29BE/5EC961EB" Ref="JP?"  Part="1" 
AR Path="/5EC961EB" Ref="JP?"  Part="1" 
AR Path="/5EC577BA/5EC961EB" Ref="JP?"  Part="1" 
F 0 "JP?" H 3800 2535 50  0000 C CNN
F 1 "reset" H 3800 2444 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3800 2300 50  0001 C CNN
F 3 "~" H 3800 2300 50  0001 C CNN
	1    3800 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EC961F1
P 6000 1800
AR Path="/5D7E29BE/5EC961F1" Ref="C?"  Part="1" 
AR Path="/5EC961F1" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC961F1" Ref="C?"  Part="1" 
F 0 "C?" V 5900 1800 50  0000 C CNN
F 1 "10nF" V 6100 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 1800 50  0001 C CNN
F 3 "~" H 6000 1800 50  0001 C CNN
	1    6000 1800
	0    1    1    0   
$EndComp
Text Notes 6200 3350 0    50   ~ 0
ADC_IN2
Text Notes 6200 3250 0    50   ~ 0
ADC_IN1\n
Text Notes 6200 3150 0    50   ~ 0
ADC_IN0
Text Notes 6750 3150 0    50   ~ 0
temp\n
Text GLabel 8000 4150 2    50   Input ~ 0
vout_load_sens
$Comp
L Device:C_Small C?
U 1 1 5EC961FC
P 8350 3350
AR Path="/5D7E29BE/5EC961FC" Ref="C?"  Part="1" 
AR Path="/5EC961FC" Ref="C?"  Part="1" 
AR Path="/5EC577BA/5EC961FC" Ref="C?"  Part="1" 
F 0 "C?" H 8500 3350 50  0000 C CNN
F 1 "100nF" H 8500 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8350 3350 50  0001 C CNN
F 3 "~" H 8350 3350 50  0001 C CNN
	1    8350 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5EC96202
P 8450 1400
F 0 "C?" H 8550 1400 50  0000 L CNN
F 1 "100nF" H 8500 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8488 1250 50  0001 C CNN
F 3 "~" H 8450 1400 50  0001 C CNN
	1    8450 1400
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP?
U 1 1 5EC96208
P 6900 1200
F 0 "TP?" H 6650 1300 50  0000 L CNN
F 1 "3.3V" H 6650 1200 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 7100 1200 50  0001 C CNN
F 3 "~" H 7100 1200 50  0001 C CNN
	1    6900 1200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 1550 8450 2000
$Comp
L power:GND #PWR?
U 1 1 5EC9620F
P 7550 2000
F 0 "#PWR?" H 7550 1750 50  0001 C CNN
F 1 "GND" H 7700 1950 50  0000 C CNN
F 2 "" H 7550 2000 50  0001 C CNN
F 3 "" H 7550 2000 50  0001 C CNN
	1    7550 2000
	-1   0    0    -1  
$EndComp
Wire Notes Line width 8
	6350 700  6350 2200
Text Notes 6350 850  0    50   ~ 10
+3.3 ref
Wire Notes Line width 8
	6350 2200 8550 2200
$Comp
L max6070:MAX6070 U?
U 1 1 5EC96218
P 7550 1500
F 0 "U?" H 7550 2087 60  0000 C CNN
F 1 "MAX6070" H 7550 1981 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 7400 1950 60  0001 C CNN
F 3 "" H 7400 1950 60  0001 C CNN
	1    7550 1500
	-1   0    0    -1  
$EndComp
Connection ~ 7550 2000
Wire Wire Line
	8000 1800 8050 1800
Wire Wire Line
	8050 1800 8050 1200
Wire Wire Line
	8050 1200 8000 1200
Connection ~ 8050 1200
$Comp
L Device:C C?
U 1 1 5EC96223
P 8150 1750
F 0 "C?" H 8250 1750 50  0000 L CNN
F 1 "100nF" H 8200 1650 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8188 1600 50  0001 C CNN
F 3 "~" H 8150 1750 50  0001 C CNN
	1    8150 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 950  8450 1200
Wire Wire Line
	7550 2000 8150 2000
Wire Wire Line
	8000 1500 8150 1500
Wire Wire Line
	8150 1500 8150 1600
Wire Wire Line
	8150 1900 8150 2000
Connection ~ 8150 2000
Wire Wire Line
	8150 2000 8450 2000
Wire Wire Line
	8050 1200 8450 1200
Connection ~ 8450 1200
Wire Wire Line
	8450 1200 8450 1250
Connection ~ 6900 1650
Wire Wire Line
	6900 2000 7550 2000
$Comp
L Device:C C?
U 1 1 5EC96235
P 6900 1850
F 0 "C?" H 7000 1850 50  0000 L CNN
F 1 "100nF" H 6950 1750 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6938 1700 50  0001 C CNN
F 3 "~" H 6900 1850 50  0001 C CNN
	1    6900 1850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7100 1650 6900 1650
Wire Wire Line
	6900 1650 6900 1700
Text Notes 6400 1450 0    39   ~ 0
OUTS close to microcontroller\n
Wire Notes Line width 8
	8550 700  6350 700 
Wire Wire Line
	9250 3450 9250 3650
Text Notes 9350 2850 0    50   ~ 0
0mV+ 10.0mV/C\n1V - 100C\n
$Comp
L power:+5V #PWR?
U 1 1 5EC96241
P 9250 2750
F 0 "#PWR?" H 9250 2600 50  0001 C CNN
F 1 "+5V" H 9265 2923 50  0000 C CNN
F 2 "" H 9250 2750 50  0001 C CNN
F 3 "" H 9250 2750 50  0001 C CNN
	1    9250 2750
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Temperature:LM35-NEB U?
U 1 1 5EC96247
P 9250 3150
AR Path="/5D7E2920/5EC96247" Ref="U?"  Part="1" 
AR Path="/5D7E2969/5EC96247" Ref="U?"  Part="1" 
AR Path="/5EC96247" Ref="U?"  Part="1" 
AR Path="/5EC577BA/5EC96247" Ref="U?"  Part="1" 
F 0 "U?" H 8920 3196 50  0000 R CNN
F 1 "LM35-NEB" H 8920 3105 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabUp" H 9300 2900 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 9250 3150 50  0001 C CNN
	1    9250 3150
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC9624D
P 9250 3650
F 0 "#PWR?" H 9250 3400 50  0001 C CNN
F 1 "GND" H 9255 3477 50  0000 C CNN
F 2 "" H 9250 3650 50  0001 C CNN
F 3 "" H 9250 3650 50  0001 C CNN
	1    9250 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2850 9250 2750
$Comp
L Device:D_Zener D?
U 1 1 5EC96254
P 8550 3300
F 0 "D?" V 8500 3350 50  0000 L CNN
F 1 "3.3V" V 8600 3350 50  0000 L CNN
F 2 "Diode_SMD:D_MiniMELF" H 8550 3300 50  0001 C CNN
F 3 "~" H 8550 3300 50  0001 C CNN
	1    8550 3300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EC9625A
P 8550 3650
F 0 "#PWR?" H 8550 3400 50  0001 C CNN
F 1 "GND" H 8555 3477 50  0000 C CNN
F 2 "" H 8550 3650 50  0001 C CNN
F 3 "" H 8550 3650 50  0001 C CNN
	1    8550 3650
	1    0    0    -1  
$EndComp
$Sheet
S 7850 5000 1100 800 
U 5EC9625C
F0 "charge" 50
F1 "buck.sch" 50
$EndSheet
$Sheet
S 7850 3950 1100 550 
U 5EC9625E
F0 "dc_load" 50
F1 "dc_load.sch" 50
$EndSheet
$EndSCHEMATC
