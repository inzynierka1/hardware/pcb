EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9800 3350 1100 800 
U 5D7E29BE
F0 "buck" 50
F1 "buck.sch" 50
F2 "charge_mosfet" I L 9800 3950 50 
F3 "charge_switch" I L 9800 3850 50 
F4 "charge_cc_pwm" I L 9800 3750 50 
F5 "charge_cv_pwm" I L 9800 3650 50 
F6 "charge_c_sens" I L 9800 3550 50 
F7 "charge_v_sens" I L 9800 3450 50 
$EndSheet
Text Notes 9300 5350 0    50   ~ 0
0mV+ 10.0mV/C\n1V - 100C\n
$Comp
L Device:C_Small C?
U 1 1 5DFC8910
P 11100 5900
AR Path="/5DF6D00C/5DFC8910" Ref="C?"  Part="1" 
AR Path="/5DFC8910" Ref="C128"  Part="1" 
F 0 "C128" V 11150 6050 50  0000 C CNN
F 1 "1uF" V 11150 5800 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 11100 5900 50  0001 C CNN
F 3 "~" H 11100 5900 50  0001 C CNN
	1    11100 5900
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DD26822
P 10700 5900
AR Path="/5D7E29BE/5DD26822" Ref="C?"  Part="1" 
AR Path="/5DD26822" Ref="C127"  Part="1" 
F 0 "C127" V 10650 5750 50  0000 C CNN
F 1 "100nF" V 10650 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10700 5900 50  0001 C CNN
F 3 "~" H 10700 5900 50  0001 C CNN
	1    10700 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DD2681C
P 10500 5900
AR Path="/5D7E29BE/5DD2681C" Ref="C?"  Part="1" 
AR Path="/5DD2681C" Ref="C126"  Part="1" 
F 0 "C126" V 10450 5750 50  0000 C CNN
F 1 "100nF" V 10450 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10500 5900 50  0001 C CNN
F 3 "~" H 10500 5900 50  0001 C CNN
	1    10500 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 5500 10300 5500
Connection ~ 10500 5500
Wire Wire Line
	10700 5500 10500 5500
Connection ~ 10700 5500
Wire Wire Line
	11100 5500 10900 5500
$Comp
L Device:C_Small C?
U 1 1 5DD2686C
P 10300 5900
AR Path="/5D7E29BE/5DD2686C" Ref="C?"  Part="1" 
AR Path="/5DD2686C" Ref="C125"  Part="1" 
F 0 "C125" V 10200 5750 50  0000 C CNN
F 1 "100nF" V 10200 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10300 5900 50  0001 C CNN
F 3 "~" H 10300 5900 50  0001 C CNN
	1    10300 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5DD26829
P 1250 3050
AR Path="/5D7E29BE/5DD26829" Ref="J?"  Part="1" 
AR Path="/5DD26829" Ref="J102"  Part="1" 
F 0 "J102" H 1358 3331 50  0000 C CNN
F 1 "prog" H 1358 3240 50  0000 C CNN
F 2 "Connector_JST:JST_EH_S4B-EH_1x04_P2.50mm_Horizontal" H 1250 3050 50  0001 C CNN
F 3 "~" H 1250 3050 50  0001 C CNN
	1    1250 3050
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DD2680A
P 10300 5400
AR Path="/5D7E29BE/5DD2680A" Ref="#PWR?"  Part="1" 
AR Path="/5DD2680A" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 10300 5250 50  0001 C CNN
F 1 "+3.3V" H 10300 5550 50  0000 C CNN
F 2 "" H 10300 5400 50  0001 C CNN
F 3 "" H 10300 5400 50  0001 C CNN
	1    10300 5400
	1    0    0    -1  
$EndComp
$Sheet
S 9800 2550 1100 550 
U 5D7E2920
F0 "dc_load" 50
F1 "dc_load.sch" 50
F2 "load_v_sens" I L 9800 2650 50 
F3 "load_c_sens" I L 9800 2750 50 
F4 "load_mosfet" I L 9800 2850 50 
F5 "load_dac" I L 9800 2950 50 
$EndSheet
Wire Notes Line
	2550 950  2550 500 
$Comp
L battery_tester-rescue:Conn_01x04-Connector_Generic J104
U 1 1 5EC20AB1
P 2100 3950
F 0 "J104" H 2100 3500 50  0000 C CNN
F 1 "fan series" H 2100 3600 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B4B-EH-A_1x04_P2.50mm_Vertical" H 2100 3950 50  0001 C CNN
F 3 "" H 2100 3950 50  0001 C CNN
	1    2100 3950
	-1   0    0    1   
$EndComp
$Comp
L power:+24V #PWR0108
U 1 1 5EE4D9D6
P 2350 3400
F 0 "#PWR0108" H 2350 3250 50  0001 C CNN
F 1 "+24V" H 2365 3573 50  0000 C CNN
F 2 "" H 2350 3400 50  0001 C CNN
F 3 "" H 2350 3400 50  0001 C CNN
	1    2350 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3400 2350 3550
$Comp
L Device:D D102
U 1 1 5EC94D50
P 2950 3700
F 0 "D102" V 2950 4000 50  0000 C CNN
F 1 "B260A-13-F" V 2850 4000 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 2950 3700 50  0001 C CNN
F 3 "~" H 2950 3700 50  0001 C CNN
	1    2950 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 3850 2500 3850
Wire Wire Line
	2300 3850 2300 3950
Wire Wire Line
	2300 3950 2500 3950
Wire Wire Line
	2350 4050 2300 4050
$Comp
L Device:R R104
U 1 1 5F414EF4
P 2800 4650
F 0 "R104" V 2700 4600 50  0000 C CNN
F 1 "100k" V 2800 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2730 4650 50  0001 C CNN
F 3 "~" H 2800 4650 50  0001 C CNN
	1    2800 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 4450 2800 4450
Wire Wire Line
	2800 4450 2800 4500
Wire Wire Line
	2350 4650 2350 4850
Wire Wire Line
	2800 4800 2800 4850
Wire Wire Line
	2800 4850 2350 4850
Connection ~ 2350 4850
Wire Wire Line
	2350 4850 2350 4900
Connection ~ 2800 4450
Wire Notes Line
	14600 5100 11850 5100
Wire Notes Line
	14600 5100 14600 4550
Wire Notes Line
	11850 5100 11850 4550
Wire Notes Line
	14600 4550 11850 4550
Text Notes 11900 5050 0    39   ~ 0
UWAGA!\n4) Przetwornica ładowanie kondensatory elektrolityczne za duże\n5) Opampy ładowania błędnie podłączone sprzężenie zwrotne (do wej nieodwracającego)\n6) Wyprowadzić miejsce na zewnętrzny czujnik temperatury do bateri (opcjonalny)\n
Wire Notes Line
	14600 5750 11850 5750
Wire Notes Line
	14600 5750 14600 5200
Wire Notes Line
	11850 5750 11850 5200
Wire Notes Line
	14600 5200 11850 5200
Text Notes 11900 5700 0    39   ~ 0
UWAGA!\n2) Dodać izolacje mas przy referancji VSSA\n
Wire Notes Line
	7900 6500 10050 6500
Wire Notes Line
	10050 6500 10050 5000
Wire Wire Line
	9200 5950 9200 6150
Wire Wire Line
	9200 5350 9200 5250
$Comp
L Sensor_Temperature:LM35-NEB U?
U 1 1 5E096405
P 9200 5650
AR Path="/5D7E2920/5E096405" Ref="U?"  Part="1" 
AR Path="/5D7E2969/5E096405" Ref="U?"  Part="1" 
AR Path="/5E096405" Ref="U107"  Part="1" 
F 0 "U107" H 8870 5696 50  0000 R CNN
F 1 "LM35-NEB" H 8870 5605 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabUp" H 9250 5400 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 9200 5650 50  0001 C CNN
	1    9200 5650
	-1   0    0    -1  
$EndComp
Wire Notes Line
	10050 5000 7900 5000
Text Notes 8050 5150 0    50   ~ 10
temp measure\n
Wire Notes Line
	7900 5000 7900 6500
Text Label 8000 5650 0    50   ~ 0
temp
Wire Wire Line
	8850 2950 9800 2950
Text Label 9700 2850 2    50   ~ 0
load_mosfet
Text Label 9650 3950 2    50   ~ 0
charge_mosfet
Wire Wire Line
	9800 2850 9700 2850
Wire Wire Line
	9800 3950 9650 3950
Text Label 9650 3850 2    50   ~ 0
charge_switch
Wire Wire Line
	9800 3850 9650 3850
Text Label 9650 3750 2    50   ~ 0
charge_cc_pwm
Wire Wire Line
	9800 3750 9650 3750
Text Label 9650 3650 2    50   ~ 0
charge_cv_pwm
Wire Wire Line
	9800 3650 9650 3650
Wire Wire Line
	10700 5500 10700 5800
Wire Wire Line
	10300 5500 10300 5800
Wire Wire Line
	10500 5500 10500 5800
Wire Wire Line
	11100 5500 11100 5800
Wire Wire Line
	10300 5400 10300 5500
Connection ~ 10300 5500
Wire Wire Line
	10300 6000 10300 6250
Wire Wire Line
	10500 6250 10500 6000
Wire Wire Line
	10500 6250 10700 6250
Wire Wire Line
	10700 6250 10700 6000
Connection ~ 10500 6250
Wire Wire Line
	11100 6000 11100 6250
Wire Wire Line
	11100 6250 10900 6250
Connection ~ 10700 6250
Wire Wire Line
	10300 6300 10300 6250
Connection ~ 10300 6250
Text Label 1550 2950 0    50   ~ 0
SWCLK
Text Label 1550 3150 0    50   ~ 0
SWDIO
Wire Wire Line
	1450 3150 1550 3150
Wire Wire Line
	1450 2950 1550 2950
Wire Wire Line
	1800 3050 1450 3050
Text Label 1550 3250 0    50   ~ 0
NRST
Wire Wire Line
	1550 3250 1450 3250
Wire Notes Line
	10100 6500 11200 6500
Wire Notes Line
	11200 6500 11200 5100
Text Notes 10150 5200 0    50   ~ 0
stm vdd filter\n
Text Notes 700  5400 0    50   ~ 0
power\n
Wire Notes Line
	600  5200 7150 5200
Wire Wire Line
	3250 6200 3350 6200
$Comp
L power:GND #PWR?
U 1 1 60A09686
P 3250 6200
AR Path="/5D7E29BE/60A09686" Ref="#PWR?"  Part="1" 
AR Path="/60A09686" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 3250 5950 50  0001 C CNN
F 1 "GND" V 3350 6150 50  0000 C CNN
F 2 "" H 3250 6200 50  0001 C CNN
F 3 "" H 3250 6200 50  0001 C CNN
	1    3250 6200
	0    1    1    0   
$EndComp
Connection ~ 4050 6200
Wire Wire Line
	4050 6200 3950 6200
Wire Wire Line
	4200 6200 4050 6200
$Comp
L Connector:TestPoint_Alt TP101
U 1 1 60900FCC
P 4200 6200
F 0 "TP101" H 4000 6350 50  0000 L CNN
F 1 "6.5V" H 4000 6250 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 4400 6200 50  0001 C CNN
F 3 "~" H 4400 6200 50  0001 C CNN
	1    4200 6200
	-1   0    0    -1  
$EndComp
Connection ~ 2200 7000
Wire Wire Line
	2200 7300 2350 7300
Connection ~ 2200 7300
Wire Wire Line
	2200 7000 2100 7000
Wire Wire Line
	1700 7300 2200 7300
Wire Wire Line
	2200 6700 2200 7000
Wire Wire Line
	2800 6700 3400 6700
Wire Wire Line
	2200 6700 2500 6700
Wire Wire Line
	2150 6700 2150 6600
Connection ~ 2150 6600
Wire Wire Line
	3400 6900 3400 6700
$Comp
L Device:R R?
U 1 1 6059E47A
P 2650 6700
AR Path="/5D7E29BE/6059E47A" Ref="R?"  Part="1" 
AR Path="/6059E47A" Ref="R103"  Part="1" 
F 0 "R103" V 2750 6700 50  0000 C CNN
F 1 "5.1k" V 2650 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2580 6700 50  0001 C CNN
F 3 "~" H 2650 6700 50  0001 C CNN
	1    2650 6700
	0    1    -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6059E2E1
P 2200 7150
AR Path="/5D7E29BE/6059E2E1" Ref="R?"  Part="1" 
AR Path="/6059E2E1" Ref="R102"  Part="1" 
F 0 "R102" V 2100 7200 50  0000 C CNN
F 1 "1.2k" V 2200 7150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2130 7150 50  0001 C CNN
F 3 "~" H 2200 7150 50  0001 C CNN
	1    2200 7150
	1    0    0    1   
$EndComp
Connection ~ 4350 6750
Connection ~ 4350 7300
Connection ~ 2950 7300
Connection ~ 3400 6900
Wire Wire Line
	3400 6900 3400 7000
Wire Wire Line
	3350 6900 3400 6900
Connection ~ 2950 6900
Wire Wire Line
	2950 6900 3050 6900
$Comp
L Device:C_Small C?
U 1 1 60329084
P 3400 7100
AR Path="/5D7E29BE/60329084" Ref="C?"  Part="1" 
AR Path="/60329084" Ref="C104"  Part="1" 
F 0 "C104" V 3300 7150 50  0000 C CNN
F 1 "100uF" V 3500 7100 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.4" H 3400 7100 50  0001 C CNN
F 3 "~" H 3400 7100 50  0001 C CNN
	1    3400 7100
	1    0    0    1   
$EndComp
$Comp
L Device:L L102
U 1 1 6019101D
P 3200 6900
F 0 "L102" V 3300 7050 50  0000 C CNN
F 1 "1 uH" V 3300 6800 50  0000 C CNN
F 2 "Inductor_SMD:L_TDK_NLV25_2.5x2.0mm" H 3200 6900 50  0001 C CNN
F 3 "~" H 3200 6900 50  0001 C CNN
	1    3200 6900
	0    -1   -1   0   
$EndComp
Connection ~ 2350 6900
Wire Wire Line
	2500 6900 2350 6900
Wire Wire Line
	2800 6900 2950 6900
Wire Wire Line
	2350 6900 2100 6900
Wire Wire Line
	2350 7300 2950 7300
Connection ~ 2350 7300
Wire Wire Line
	2350 7200 2350 7300
Wire Wire Line
	2100 6700 2150 6700
Connection ~ 2150 6500
Wire Wire Line
	2150 6600 2150 6500
Wire Wire Line
	2100 6600 2150 6600
Wire Wire Line
	2150 6500 2100 6500
Wire Wire Line
	2150 6100 2150 6500
Wire Wire Line
	1850 6100 2150 6100
Wire Wire Line
	1250 6500 950  6500
Connection ~ 1250 6500
Wire Wire Line
	1250 6100 1250 6500
Wire Wire Line
	1550 6100 1250 6100
$Comp
L Device:R R?
U 1 1 60003979
P 1700 6100
AR Path="/5D7E29BE/60003979" Ref="R?"  Part="1" 
AR Path="/60003979" Ref="R101"  Part="1" 
F 0 "R101" V 1800 6100 50  0000 C CNN
F 1 "330" V 1700 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1630 6100 50  0001 C CNN
F 3 "~" H 1700 6100 50  0001 C CNN
	1    1700 6100
	0    1    -1   0   
$EndComp
Wire Wire Line
	1300 7300 1700 7300
Connection ~ 1300 7300
Wire Wire Line
	1300 7150 1300 7300
Wire Wire Line
	1300 6900 1300 6950
$Comp
L Device:C_Small C?
U 1 1 5FFB47ED
P 1300 7050
AR Path="/5D7E29BE/5FFB47ED" Ref="C?"  Part="1" 
AR Path="/5FFB47ED" Ref="C102"  Part="1" 
F 0 "C102" H 1100 7050 50  0000 C CNN
F 1 "470p" H 1150 7150 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1300 7050 50  0001 C CNN
F 3 "~" H 1300 7050 50  0001 C CNN
	1    1300 7050
	1    0    0    1   
$EndComp
Connection ~ 950  6500
Wire Wire Line
	1300 6500 1250 6500
Wire Wire Line
	950  7300 1300 7300
$Comp
L Regulator_Switching:MC34063AD U101
U 1 1 5FF19992
P 1700 6700
F 0 "U101" H 1700 7167 50  0000 C CNN
F 1 "MC34063AD" H 1700 7076 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 1750 6250 50  0001 L CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MC34063A-D.PDF" H 2200 6600 50  0001 C CNN
	1    1700 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F9AC4F7
P 3800 6200
AR Path="/5D7E29BE/5F9AC4F7" Ref="R?"  Part="1" 
AR Path="/5F9AC4F7" Ref="R105"  Part="1" 
F 0 "R105" V 3700 6250 50  0000 C CNN
F 1 "220" V 3800 6200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3730 6200 50  0001 C CNN
F 3 "~" H 3800 6200 50  0001 C CNN
	1    3800 6200
	0    -1   1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F9AC4F1
P 3500 6200
AR Path="/5D7E29BE/5F9AC4F1" Ref="D?"  Part="1" 
AR Path="/5F9AC4F1" Ref="D106"  Part="1" 
F 0 "D106" H 3500 6300 50  0000 R CNN
F 1 "LED" H 3550 6100 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3500 6200 50  0001 C CNN
F 3 "~" H 3500 6200 50  0001 C CNN
	1    3500 6200
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E063713
P 4350 7050
AR Path="/5D7E29BE/5E063713" Ref="C?"  Part="1" 
AR Path="/5E063713" Ref="C107"  Part="1" 
F 0 "C107" V 4400 7200 50  0000 C CNN
F 1 "10uF" V 4500 7200 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.3" H 4350 7050 50  0001 C CNN
F 3 "~" H 4350 7050 50  0001 C CNN
	1    4350 7050
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E0639CA
P 5050 7050
AR Path="/5D7E29BE/5E0639CA" Ref="C?"  Part="1" 
AR Path="/5E0639CA" Ref="C108"  Part="1" 
F 0 "C108" H 5250 7050 50  0000 C CNN
F 1 "22uF" H 5250 7150 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_4x5.3" H 5050 7050 50  0001 C CNN
F 3 "~" H 5050 7050 50  0001 C CNN
	1    5050 7050
	1    0    0    1   
$EndComp
Connection ~ 6850 5600
Wire Wire Line
	6850 5500 6850 5600
$Comp
L power:-5V #PWR0123
U 1 1 5E078FAF
P 6850 5500
F 0 "#PWR0123" H 6850 5600 50  0001 C CNN
F 1 "-5V" H 6865 5673 50  0000 C CNN
F 2 "" H 6850 5500 50  0001 C CNN
F 3 "" H 6850 5500 50  0001 C CNN
	1    6850 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9A7F5E
P 6550 5950
AR Path="/5D7E29BE/5E9A7F5E" Ref="C?"  Part="1" 
AR Path="/5E9A7F5E" Ref="C112"  Part="1" 
F 0 "C112" H 6650 5850 50  0000 C CNN
F 1 "1uF" H 6650 6050 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6550 5950 50  0001 C CNN
F 3 "~" H 6550 5950 50  0001 C CNN
	1    6550 5950
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9A81F9
P 6850 5950
AR Path="/5D7E29BE/5E9A81F9" Ref="C?"  Part="1" 
AR Path="/5E9A81F9" Ref="C114"  Part="1" 
F 0 "C114" H 7050 5950 50  0000 C CNN
F 1 "1uF" H 7050 6050 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6850 5950 50  0001 C CNN
F 3 "~" H 6850 5950 50  0001 C CNN
	1    6850 5950
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E9A7AA9
P 5650 5950
AR Path="/5D7E29BE/5E9A7AA9" Ref="C?"  Part="1" 
AR Path="/5E9A7AA9" Ref="C109"  Part="1" 
F 0 "C109" H 5500 5950 50  0000 C CNN
F 1 "1uF" H 5500 6050 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5650 5950 50  0001 C CNN
F 3 "~" H 5650 5950 50  0001 C CNN
	1    5650 5950
	1    0    0    1   
$EndComp
Wire Wire Line
	6550 6300 6500 6300
Wire Wire Line
	6550 6050 6550 6300
Wire Wire Line
	6550 5700 6500 5700
Wire Wire Line
	6550 5850 6550 5700
Wire Wire Line
	6850 5600 6500 5600
Wire Wire Line
	6850 5850 6850 5600
Wire Wire Line
	5650 5600 5400 5600
Connection ~ 5650 5600
Wire Wire Line
	5650 5850 5650 5600
Connection ~ 5650 6300
Wire Wire Line
	5650 6050 5650 6300
Wire Wire Line
	5650 6300 5650 6450
Wire Wire Line
	5650 6300 5700 6300
Wire Wire Line
	5050 6650 5050 6750
Connection ~ 5050 6750
Connection ~ 5400 6750
Wire Wire Line
	5400 6750 5050 6750
Wire Wire Line
	5400 5600 5400 6750
Wire Wire Line
	5700 5600 5650 5600
$Comp
L TPS60400:TPS60403 U104
U 1 1 5E5635B9
P 6100 5950
F 0 "U104" H 6100 6587 60  0000 C CNN
F 1 "TPS60403" H 6100 6481 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6100 6458 30  0001 C CNN
F 3 "" H 6100 5950 50  0000 C CNN
	1    6100 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6750 5800 6750
Wire Wire Line
	5700 7300 6100 7300
Connection ~ 5700 7300
Wire Wire Line
	5700 7150 5700 7300
Wire Wire Line
	5700 6750 5700 6950
Connection ~ 5700 6750
Wire Wire Line
	5400 6750 5700 6750
Wire Wire Line
	5400 7300 5700 7300
Connection ~ 5400 7300
Wire Wire Line
	5400 7350 5400 7300
Wire Wire Line
	4950 6750 5050 6750
Wire Wire Line
	5050 6950 5050 6750
$Comp
L Device:C_Small C?
U 1 1 5E3B3A2F
P 5700 7050
AR Path="/5D7E29BE/5E3B3A2F" Ref="C?"  Part="1" 
AR Path="/5E3B3A2F" Ref="C110"  Part="1" 
F 0 "C110" H 5900 7050 50  0000 C CNN
F 1 "1uF" H 5900 7150 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5700 7050 50  0001 C CNN
F 3 "~" H 5700 7050 50  0001 C CNN
	1    5700 7050
	1    0    0    1   
$EndComp
Wire Wire Line
	6500 6750 6650 6750
Connection ~ 6500 6750
Wire Wire Line
	6500 6950 6500 6750
Connection ~ 6100 7300
Wire Wire Line
	6500 7300 6500 7150
Wire Wire Line
	6100 7300 6500 7300
$Comp
L Device:C_Small C?
U 1 1 5E38509A
P 6500 7050
AR Path="/5D7E29BE/5E38509A" Ref="C?"  Part="1" 
AR Path="/5E38509A" Ref="C111"  Part="1" 
F 0 "C111" H 6700 7050 50  0000 C CNN
F 1 "1uF" H 6700 7150 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6500 7050 50  0001 C CNN
F 3 "~" H 6500 7050 50  0001 C CNN
	1    6500 7050
	1    0    0    1   
$EndComp
Wire Wire Line
	6650 6700 6650 6750
$Comp
L power:+3.3V #PWR0122
U 1 1 5E36D5D9
P 6650 6700
F 0 "#PWR0122" H 6650 6550 50  0001 C CNN
F 1 "+3.3V" H 6550 6900 50  0000 L CNN
F 2 "" H 6650 6700 50  0001 C CNN
F 3 "" H 6650 6700 50  0001 C CNN
	1    6650 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 6750 6500 6750
Connection ~ 5050 7300
Wire Wire Line
	5050 7300 5400 7300
Wire Wire Line
	6100 7050 6100 7300
$Comp
L Regulator_Linear:AZ1117-3.3 U105
U 1 1 5E2A1FEB
P 6100 6750
F 0 "U105" H 6100 6992 50  0000 C CNN
F 1 "AZ1117-3.3" H 6100 6901 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 6100 7000 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 6100 6750 50  0001 C CNN
	1    6100 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 7300 4350 7300
Wire Wire Line
	5050 7300 4650 7300
Connection ~ 4650 7300
Wire Wire Line
	4650 7300 4650 7050
Wire Wire Line
	4350 7300 4350 7150
Wire Wire Line
	4350 6950 4350 6750
Wire Wire Line
	5050 7150 5050 7300
$Comp
L power:+5V #PWR?
U 1 1 5E04FCE7
P 5050 6650
AR Path="/5D7E29BE/5E04FCE7" Ref="#PWR?"  Part="1" 
AR Path="/5E04FCE7" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 5050 6500 50  0001 C CNN
F 1 "+5V" H 5050 6850 50  0000 C CNN
F 2 "" H 5050 6650 50  0001 C CNN
F 3 "" H 5050 6650 50  0001 C CNN
	1    5050 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  7150 950  7300
$Comp
L Regulator_Linear:AZ1117-5.0 U102
U 1 1 5DFF1660
P 4650 6750
F 0 "U102" H 4650 6992 50  0000 C CNN
F 1 "AZ1117-5.0" H 4650 6901 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 4650 7000 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 4650 6750 50  0001 C CNN
	1    4650 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:L L101
U 1 1 5DEBF570
P 2650 6900
F 0 "L101" V 2750 7050 50  0000 C CNN
F 1 "220 uH" V 2750 6750 50  0000 C CNN
F 2 "Inductor_SMD:L_Sunlord_MWSA0518_5.4x5.2mm" H 2650 6900 50  0001 C CNN
F 3 "~" H 2650 6900 50  0001 C CNN
	1    2650 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 7000 2950 6900
Wire Wire Line
	2950 7300 2950 7200
Connection ~ 1700 7300
Wire Wire Line
	1700 7200 1700 7300
Wire Wire Line
	950  6500 950  6950
Wire Wire Line
	700  6500 950  6500
$Comp
L power:+24V #PWR0102
U 1 1 5DD3B5DB
P 700 6500
F 0 "#PWR0102" H 700 6350 50  0001 C CNN
F 1 "+24V" H 715 6673 50  0000 C CNN
F 2 "" H 700 6500 50  0001 C CNN
F 3 "" H 700 6500 50  0001 C CNN
	1    700  6500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DD2BE0B
P 2950 7100
AR Path="/5D7E29BE/5DD2BE0B" Ref="C?"  Part="1" 
AR Path="/5DD2BE0B" Ref="C103"  Part="1" 
F 0 "C103" V 2850 7100 50  0000 C CNN
F 1 "470uF" V 3050 7100 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_8x10" H 2950 7100 50  0001 C CNN
F 3 "~" H 2950 7100 50  0001 C CNN
	1    2950 7100
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DD2B9D6
P 950 7050
AR Path="/5D7E29BE/5DD2B9D6" Ref="C?"  Part="1" 
AR Path="/5DD2B9D6" Ref="C101"  Part="1" 
F 0 "C101" H 750 7050 50  0000 C CNN
F 1 "100uF" H 800 7150 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 950 7050 50  0001 C CNN
F 3 "~" H 950 7050 50  0001 C CNN
	1    950  7050
	1    0    0    1   
$EndComp
$Comp
L Device:D_Schottky D101
U 1 1 5DD080DD
P 2350 7050
F 0 "D101" V 2300 7250 50  0000 C CNN
F 1 "1N5819HW" V 2450 7300 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 2350 7050 50  0001 C CNN
F 3 "~" H 2350 7050 50  0001 C CNN
	1    2350 7050
	0    1    1    0   
$EndComp
Wire Notes Line
	600  7600 600  5200
Wire Notes Line
	7150 5200 7150 6500
Wire Notes Line
	7150 6500 6950 6500
Wire Notes Line
	6950 6500 6950 7600
Wire Notes Line
	600  7600 6950 7600
Wire Notes Line
	11200 5100 10100 5100
Wire Notes Line
	10100 5100 10100 6500
Text Label 4850 1950 2    50   ~ 0
NRST
Text Label 6350 4350 0    50   ~ 0
SWDIO
Text Label 6350 4450 0    50   ~ 0
SWCLK
Text Label 4450 4450 2    50   ~ 0
fan_pwm
Text Label 4800 2450 2    50   ~ 0
I2C_CLK
Text Label 4800 2350 2    50   ~ 0
I2C_SDA
Text Label 4400 3850 2    50   ~ 0
charge_switch
Text Label 4450 4050 2    50   ~ 0
charge_cv_pwm
Text Label 4450 4150 2    50   ~ 0
charge_cc_pwm
Text Label 4450 3150 2    50   ~ 0
charge_mosfet
Text Label 4450 3250 2    50   ~ 0
load_mosfet
Text Label 8150 3250 0    50   ~ 0
charge_v_sens
$Comp
L Device:R R112
U 1 1 5E101593
P 7950 3250
F 0 "R112" V 7900 3000 50  0000 C CNN
F 1 "R_opt" V 7950 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3250 50  0001 C CNN
F 3 "~" H 7950 3250 50  0001 C CNN
	1    7950 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	8100 3350 8150 3350
Text Label 8150 3650 0    50   ~ 0
charge_c_sens
$Comp
L Device:R R?
U 1 1 5DD268FB
P 4050 3650
AR Path="/5D7E29BE/5DD268FB" Ref="R?"  Part="1" 
AR Path="/5DD268FB" Ref="R108"  Part="1" 
F 0 "R108" V 4150 3650 50  0000 C CNN
F 1 "680" V 4050 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3980 3650 50  0001 C CNN
F 3 "~" H 4050 3650 50  0001 C CNN
	1    4050 3650
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DD268F5
P 4350 3650
AR Path="/5D7E29BE/5DD268F5" Ref="D?"  Part="1" 
AR Path="/5DD268F5" Ref="D107"  Part="1" 
F 0 "D107" H 4500 3750 50  0000 R CNN
F 1 "LED" H 4450 3850 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4350 3650 50  0001 C CNN
F 3 "~" H 4350 3650 50  0001 C CNN
	1    4350 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5ED05729
P 3850 3650
AR Path="/5D7E29BE/5ED05729" Ref="#PWR?"  Part="1" 
AR Path="/5ED05729" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 3850 3500 50  0001 C CNN
F 1 "+3.3V" V 3950 3800 50  0000 C CNN
F 2 "" H 3850 3650 50  0001 C CNN
F 3 "" H 3850 3650 50  0001 C CNN
	1    3850 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 3650 3900 3650
Text Label 8150 3150 0    50   ~ 0
load_c_sens
Text Label 8150 3050 0    50   ~ 0
load_v_sens
Text Label 8150 3350 0    50   ~ 0
temp
Wire Notes Line width 8
	10950 600  10950 2100
Wire Wire Line
	4350 2150 4600 2150
Wire Wire Line
	4350 2150 4350 2500
Wire Wire Line
	5450 1700 5550 1700
Wire Wire Line
	7700 3650 7800 3650
$Comp
L Device:R R113
U 1 1 5E0B07E0
P 7950 3350
F 0 "R113" V 7900 3100 50  0000 C CNN
F 1 "R_opt" V 7950 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3350 50  0001 C CNN
F 3 "~" H 7950 3350 50  0001 C CNN
	1    7950 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 3350 7800 3350
Wire Wire Line
	6900 4100 6900 4050
Wire Wire Line
	6950 1000 6950 1550
Wire Wire Line
	5850 1250 7150 1250
Connection ~ 5850 1550
Connection ~ 6950 1550
Wire Wire Line
	5850 1250 5850 1550
$Comp
L power:GNDA #PWR0120
U 1 1 5E371B96
P 5750 4900
F 0 "#PWR0120" H 5750 4650 50  0001 C CNN
F 1 "GNDA" H 5750 4750 50  0000 C CNN
F 2 "" H 5750 4900 50  0001 C CNN
F 3 "" H 5750 4900 50  0001 C CNN
	1    5750 4900
	1    0    0    -1  
$EndComp
Text Notes 4550 3850 0    50   ~ 0
GPIO_out
Text Notes 4550 3250 0    50   ~ 0
GPIO_out
Text Notes 4550 3150 0    50   ~ 0
GPIO_out
Text Notes 4550 3650 0    50   ~ 0
GPIO_out
Text Notes 4500 4050 0    50   ~ 0
TIM2_CH3
Text Notes 4500 4150 0    50   ~ 0
TIM2_CH4\n
Text Notes 4500 4450 0    50   ~ 0
TIM15_CH1
Wire Wire Line
	7400 3850 7400 3350
Wire Wire Line
	7700 3650 7700 3850
$Comp
L Device:C_Small C?
U 1 1 5E4D0F99
P 7700 3950
AR Path="/5D7E2920/5E4D0F99" Ref="C?"  Part="1" 
AR Path="/5DAD577F/5E4D0F99" Ref="C?"  Part="1" 
AR Path="/5E4D0F99" Ref="C119"  Part="1" 
F 0 "C119" V 7800 3850 50  0000 C CNN
F 1 "C_opt" V 7800 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7700 3950 50  0001 C CNN
F 3 "~" H 7700 3950 50  0001 C CNN
	1    7700 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7700 4050 7700 4100
$Comp
L Device:C_Small C?
U 1 1 5E02D5D6
P 7400 3950
AR Path="/5D7E2920/5E02D5D6" Ref="C?"  Part="1" 
AR Path="/5DAD577F/5E02D5D6" Ref="C?"  Part="1" 
AR Path="/5E02D5D6" Ref="C118"  Part="1" 
F 0 "C118" V 7500 3850 50  0000 C CNN
F 1 "C_opt" V 7500 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7400 3950 50  0001 C CNN
F 3 "~" H 7400 3950 50  0001 C CNN
	1    7400 3950
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R114
U 1 1 5E1365AD
P 7950 3650
F 0 "R114" V 7900 3400 50  0000 C CNN
F 1 "R_opt" V 7950 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3650 50  0001 C CNN
F 3 "~" H 7950 3650 50  0001 C CNN
	1    7950 3650
	0    1    1    0   
$EndComp
Text Notes 6250 3150 0    50   ~ 0
ADC_IN2
Text Notes 6250 3350 0    50   ~ 0
ADC_IN4
$Comp
L Device:R R110
U 1 1 5E0829A3
P 7950 3050
F 0 "R110" V 7900 2800 50  0000 C CNN
F 1 "R_opt" V 7950 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3050 50  0001 C CNN
F 3 "~" H 7950 3050 50  0001 C CNN
	1    7950 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 4050 7400 4100
$Comp
L power:+5V #PWR?
U 1 1 5E907F78
P 10400 900
AR Path="/5D7E29BE/5E907F78" Ref="#PWR?"  Part="1" 
AR Path="/5E907F78" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 10400 750 50  0001 C CNN
F 1 "+5V" H 10400 1100 50  0000 C CNN
F 2 "" H 10400 900 50  0001 C CNN
F 3 "" H 10400 900 50  0001 C CNN
	1    10400 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4100 6650 4050
Wire Wire Line
	7150 4050 7150 4100
Wire Wire Line
	5750 1700 5650 1700
Connection ~ 5650 1700
Wire Wire Line
	5650 1700 5550 1700
Connection ~ 5550 1700
Wire Wire Line
	5550 1350 5550 1700
$Comp
L power:+3.3V #PWR?
U 1 1 5DD26803
P 5550 1350
AR Path="/5D7E29BE/5DD26803" Ref="#PWR?"  Part="1" 
AR Path="/5DD26803" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 5550 1200 50  0001 C CNN
F 1 "+3.3V" H 5550 1500 50  0000 C CNN
F 2 "" H 5550 1350 50  0001 C CNN
F 3 "" H 5550 1350 50  0001 C CNN
	1    5550 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DD26864
P 4750 2150
AR Path="/5D7E29BE/5DD26864" Ref="R?"  Part="1" 
AR Path="/5DD26864" Ref="R109"  Part="1" 
F 0 "R109" V 4650 2150 50  0000 C CNN
F 1 "10k" V 4750 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4680 2150 50  0001 C CNN
F 3 "~" H 4750 2150 50  0001 C CNN
	1    4750 2150
	0    -1   1    0   
$EndComp
Wire Wire Line
	3850 2500 4050 2500
Connection ~ 4050 2500
Connection ~ 4350 2500
Wire Wire Line
	4350 2550 4350 2500
Wire Wire Line
	4050 1450 4050 1550
Wire Wire Line
	4350 2500 4050 2500
Wire Wire Line
	4050 2500 4050 2300
Wire Wire Line
	3850 2400 3850 2500
Wire Wire Line
	4050 1950 4050 2100
Connection ~ 4050 1950
Wire Wire Line
	3850 1950 4050 1950
Wire Wire Line
	4050 1850 4050 1950
Wire Wire Line
	3850 1950 3850 2000
$Comp
L power:+3.3V #PWR?
U 1 1 5DD26841
P 4050 1450
AR Path="/5D7E29BE/5DD26841" Ref="#PWR?"  Part="1" 
AR Path="/5DD26841" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 4050 1300 50  0001 C CNN
F 1 "+3.3V" H 4050 1600 50  0000 C CNN
F 2 "" H 4050 1450 50  0001 C CNN
F 3 "" H 4050 1450 50  0001 C CNN
	1    4050 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DD2683B
P 4050 1700
AR Path="/5D7E29BE/5DD2683B" Ref="R?"  Part="1" 
AR Path="/5DD2683B" Ref="R107"  Part="1" 
F 0 "R107" V 3950 1700 50  0000 C CNN
F 1 "10k" V 4050 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 1700 50  0001 C CNN
F 3 "~" H 4050 1700 50  0001 C CNN
	1    4050 1700
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DD26835
P 4050 2200
AR Path="/5D7E29BE/5DD26835" Ref="C?"  Part="1" 
AR Path="/5DD26835" Ref="C105"  Part="1" 
F 0 "C105" V 4000 2100 50  0000 C CNN
F 1 "100nF" V 4150 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4050 2200 50  0001 C CNN
F 3 "~" H 4050 2200 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5DD2682F
P 3850 2200
AR Path="/5D7E29BE/5DD2682F" Ref="JP?"  Part="1" 
AR Path="/5DD2682F" Ref="JP102"  Part="1" 
F 0 "JP102" H 3850 2435 50  0000 C CNN
F 1 "reset" H 3850 2344 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3850 2200 50  0001 C CNN
F 3 "~" H 3850 2200 50  0001 C CNN
	1    3850 2200
	0    -1   -1   0   
$EndComp
Text Notes 6250 3250 0    50   ~ 0
ADC_IN3
Text Notes 6250 3050 0    50   ~ 0
ADC_IN1\n
$Comp
L Device:C C121
U 1 1 5D9E9FCA
P 8500 1300
F 0 "C121" H 8600 1300 50  0000 L CNN
F 1 "100nF" H 8550 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8538 1150 50  0001 C CNN
F 3 "~" H 8500 1300 50  0001 C CNN
	1    8500 1300
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Alt TP102
U 1 1 5D9E9FC4
P 6950 1000
F 0 "TP102" H 6650 1150 50  0000 L CNN
F 1 "3.3_ref" H 6650 1050 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 7150 1000 50  0001 C CNN
F 3 "~" H 7150 1000 50  0001 C CNN
	1    6950 1000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8500 1450 8500 1900
Wire Notes Line width 8
	6400 600  6400 2100
Text Notes 6400 750  0    50   ~ 10
+3.3 ref
Wire Notes Line width 8
	6400 2100 10950 2100
$Comp
L max6070:MAX6070 U106
U 1 1 5D9E9FAC
P 7600 1400
F 0 "U106" H 7600 1987 60  0000 C CNN
F 1 "MAX6070" H 7600 1881 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 7450 1850 60  0001 C CNN
F 3 "" H 7450 1850 60  0001 C CNN
	1    7600 1400
	-1   0    0    -1  
$EndComp
Connection ~ 7600 1900
Wire Wire Line
	7600 1900 8200 1900
Wire Wire Line
	6950 1900 7600 1900
Wire Wire Line
	8050 1700 8100 1700
Wire Wire Line
	8100 1700 8100 1100
Connection ~ 8100 1100
Wire Wire Line
	8100 1100 8050 1100
Wire Wire Line
	8100 1100 8500 1100
$Comp
L Device:C C120
U 1 1 5D9E9F9A
P 8200 1650
F 0 "C120" H 8300 1650 50  0000 L CNN
F 1 "100nF" H 8250 1550 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8238 1500 50  0001 C CNN
F 3 "~" H 8200 1650 50  0001 C CNN
	1    8200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 900  10400 1100
Wire Wire Line
	8050 1400 8200 1400
Wire Wire Line
	8200 1400 8200 1500
Wire Wire Line
	8200 1800 8200 1900
Connection ~ 8200 1900
Wire Wire Line
	8200 1900 8500 1900
Wire Wire Line
	8500 1100 8500 1150
$Comp
L Device:C C116
U 1 1 5D9E9F84
P 6950 1750
F 0 "C116" H 7050 1750 50  0000 L CNN
F 1 "100nF" H 7000 1650 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6988 1600 50  0001 C CNN
F 3 "~" H 6950 1750 50  0001 C CNN
	1    6950 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7150 1550 6950 1550
Wire Wire Line
	6950 1550 6950 1600
Text Notes 6450 1350 0    39   ~ 0
OUTS close to microcontroller\n
Wire Notes Line width 8
	10950 600  6400 600 
Wire Wire Line
	700  900  900  900 
Connection ~ 700  900 
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E4338BB
P 700 900
F 0 "#FLG0101" H 700 975 50  0001 C CNN
F 1 "PWR_FLAG" H 700 1073 50  0001 C CNN
F 2 "" H 700 900 50  0001 C CNN
F 3 "~" H 700 900 50  0001 C CNN
	1    700  900 
	1    0    0    -1  
$EndComp
Connection ~ 900  2000
Wire Wire Line
	900  2100 900  2000
Wire Wire Line
	1000 2100 900  2100
Wire Wire Line
	1600 2200 1650 2200
Wire Wire Line
	900  1000 900  900 
Wire Wire Line
	1000 1000 900  1000
Connection ~ 900  900 
Wire Wire Line
	1000 900  900  900 
Wire Wire Line
	900  900  900  800 
Wire Wire Line
	600  900  700  900 
Wire Wire Line
	900  800  1000 800 
Connection ~ 1600 2400
Wire Wire Line
	1600 2500 1600 2400
Wire Wire Line
	1500 2500 1600 2500
Wire Wire Line
	1500 2400 1600 2400
Wire Wire Line
	1500 2300 1600 2300
Wire Wire Line
	1600 2300 1600 2400
Connection ~ 1600 2300
Wire Wire Line
	1600 2200 1600 2300
Wire Wire Line
	1500 2200 1600 2200
Wire Wire Line
	1500 1200 1600 1200
Connection ~ 1600 1500
Wire Wire Line
	1500 1600 1600 1600
Wire Wire Line
	1600 1500 1600 1600
Connection ~ 1600 2000
Wire Wire Line
	1600 2000 1600 2100
Connection ~ 1600 1400
Wire Wire Line
	1600 1500 1500 1500
Wire Wire Line
	1600 1400 1600 1500
Connection ~ 1600 1000
Wire Wire Line
	1600 1100 1600 1000
Wire Wire Line
	1500 1100 1600 1100
Wire Wire Line
	900  1900 900  2000
Wire Wire Line
	1000 1900 900  1900
Wire Wire Line
	1000 1800 900  1800
Wire Wire Line
	900  2400 900  2300
Connection ~ 900  2400
Connection ~ 900  2300
Wire Wire Line
	1000 2400 900  2400
Wire Wire Line
	900  2300 900  2200
Wire Wire Line
	1000 2300 900  2300
Wire Wire Line
	900  2200 1000 2200
Wire Wire Line
	1600 1900 1600 2000
Wire Wire Line
	1500 1900 1600 1900
Wire Wire Line
	1500 1800 1600 1800
Wire Wire Line
	1500 1700 1600 1700
Wire Wire Line
	1600 1700 1600 1800
Wire Wire Line
	900  1400 900  1300
Connection ~ 900  1300
Wire Wire Line
	1000 1400 900  1400
Wire Wire Line
	1000 1300 900  1300
Wire Wire Line
	1000 1200 900  1200
Wire Wire Line
	1000 1100 900  1100
Wire Wire Line
	900  1700 900  1600
Wire Wire Line
	1000 1700 900  1700
Connection ~ 900  1600
Wire Wire Line
	1600 1400 1500 1400
Wire Wire Line
	1500 1000 1600 1000
Wire Wire Line
	1500 900  1600 900 
Wire Wire Line
	1000 1600 900  1600
Wire Wire Line
	900  2500 1000 2500
Wire Wire Line
	900  2500 900  2400
Wire Wire Line
	900  1600 900  1500
Wire Wire Line
	900  1500 1000 1500
Wire Wire Line
	1500 2100 1600 2100
Wire Wire Line
	1600 2000 1500 2000
Wire Wire Line
	900  2000 1000 2000
Wire Wire Line
	1600 1300 1600 1400
Wire Wire Line
	1500 1300 1600 1300
Wire Wire Line
	1600 900  1600 1000
Connection ~ 1600 900 
Wire Wire Line
	1600 800  1600 900 
Wire Wire Line
	1500 800  1600 800 
Wire Wire Line
	900  1200 900  1300
Wire Wire Line
	1600 1200 1600 1300
Connection ~ 1600 1300
Wire Wire Line
	1600 1600 1600 1700
Connection ~ 1600 1600
Connection ~ 1600 1700
Wire Wire Line
	1600 2100 1600 2200
Connection ~ 1600 2100
Connection ~ 1600 2200
Wire Wire Line
	900  2200 900  2100
Connection ~ 900  2200
Connection ~ 900  2100
Wire Wire Line
	900  1800 900  1700
Connection ~ 900  1700
Wire Wire Line
	900  1500 900  1400
Connection ~ 900  1500
Connection ~ 900  1400
Wire Wire Line
	900  1100 900  1000
Connection ~ 900  1000
$Comp
L schematic_pci:Conn_02x18_for_PCIE J101
U 1 1 5E07047B
P 1200 1600
F 0 "J101" H 1250 2617 50  0000 C CNN
F 1 "Conn_02x18_for_PCIE" H 1250 2526 50  0000 C CNN
F 2 "Connector_PCBEdge:BUS_PCIexpress" H 1200 1600 50  0001 C CNN
F 3 "~" H 1200 1600 50  0001 C CNN
	1    1200 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1500 1600 1500
Text Label 1650 2200 0    50   ~ 0
I2C_SDA
Text Label 1650 1500 0    50   ~ 0
I2C_CLK
Wire Notes Line
	2550 500  3150 500 
Connection ~ 3250 1100
Wire Wire Line
	3250 1150 3250 1100
Wire Wire Line
	3350 1100 3250 1100
Text GLabel 3350 1100 2    50   Input ~ 0
bat_plus
Connection ~ 2900 1100
Wire Wire Line
	2900 1150 2900 1100
Wire Wire Line
	2900 950  2900 1100
Wire Wire Line
	3250 1500 3250 1450
Wire Wire Line
	2900 1100 3250 1100
$Comp
L Diode:1.5KExxA D?
U 1 1 5EB9D6BF
P 3250 1300
AR Path="/5D7E29BE/5EB9D6BF" Ref="D?"  Part="1" 
AR Path="/5EB9D6BF" Ref="D105"  Part="1" 
F 0 "D105" H 3200 1200 50  0000 L CNN
F 1 "SMCJ22A" H 3100 1100 50  0000 L CNN
F 2 "Diode_SMD:D_SMC_Handsoldering" H 3250 1100 50  0001 C CNN
F 3 "https://www.vishay.com/docs/88301/15ke.pdf" H 3200 1300 50  0001 C CNN
	1    3250 1300
	0    -1   1    0   
$EndComp
Wire Notes Line
	3150 950  2550 950 
Wire Notes Line
	3150 500  3150 950 
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EB9D6A3
P 2900 750
AR Path="/5D7E29BE/5EB9D6A3" Ref="J?"  Part="1" 
AR Path="/5EB9D6A3" Ref="J105"  Part="1" 
F 0 "J105" V 3100 750 50  0000 R CNN
F 1 "battery_load" V 3000 950 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P7.62mm_Drill2.5mm" H 2900 750 50  0001 C CNN
F 3 "~" H 2900 750 50  0001 C CNN
	1    2900 750 
	0    1    -1   0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5EB9D6B9
P 2900 1300
AR Path="/5D7E29BE/5EB9D6B9" Ref="D?"  Part="1" 
AR Path="/5EB9D6B9" Ref="D104"  Part="1" 
F 0 "D104" H 2850 1200 50  0000 L CNN
F 1 "MLV0152" H 2750 1100 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2900 1300 50  0001 C CNN
F 3 "~" H 2900 1300 50  0001 C CNN
	1    2900 1300
	0    -1   1    0   
$EndComp
Wire Wire Line
	3250 1500 2900 1500
Wire Wire Line
	2900 1500 2900 1450
Wire Wire Line
	2900 1500 2800 1500
Connection ~ 2900 1500
Connection ~ 2800 1500
Wire Wire Line
	2800 1500 2800 1550
Wire Wire Line
	8650 2650 9800 2650
Wire Wire Line
	8100 3050 8650 3050
Wire Wire Line
	8750 2750 8750 3150
Wire Wire Line
	8750 2750 9800 2750
Wire Wire Line
	8100 3150 8750 3150
Wire Wire Line
	8950 3450 8950 3250
Wire Wire Line
	8950 3450 9800 3450
Wire Wire Line
	8100 3250 8950 3250
Wire Wire Line
	8950 3550 8950 3650
Wire Wire Line
	8950 3550 9800 3550
Wire Wire Line
	8100 3650 8950 3650
Text Label 6800 1550 2    50   ~ 0
3.3_ref
Wire Wire Line
	5850 1550 6950 1550
Wire Wire Line
	8000 5650 8550 5650
$Comp
L Device:R R?
U 1 1 61096718
P 8550 5800
AR Path="/5D7E29BE/61096718" Ref="R?"  Part="1" 
AR Path="/61096718" Ref="R115"  Part="1" 
F 0 "R115" V 8450 5750 50  0000 C CNN
F 1 "75" V 8550 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 8480 5800 50  0001 C CNN
F 3 "~" H 8550 5800 50  0001 C CNN
	1    8550 5800
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C122
U 1 1 61097A03
P 8550 6100
F 0 "C122" H 8650 6100 50  0000 L CNN
F 1 "100nF" H 8600 6000 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8588 5950 50  0001 C CNN
F 3 "~" H 8550 6100 50  0001 C CNN
	1    8550 6100
	-1   0    0    -1  
$EndComp
Connection ~ 8550 5650
Wire Wire Line
	8550 5650 8800 5650
$Comp
L michal_lib:+6.5V #PWR0113
U 1 1 61243318
P 4050 6200
F 0 "#PWR0113" H 4050 6050 50  0001 C CNN
F 1 "+6.5V" H 4065 6373 50  0000 C CNN
F 2 "" H 4050 6200 50  0001 C CNN
F 3 "" H 4050 6200 50  0001 C CNN
	1    4050 6200
	1    0    0    -1  
$EndComp
Connection ~ 7400 3350
Connection ~ 7700 3650
Wire Wire Line
	8650 2650 8650 3050
$Comp
L Device:C_Small C?
U 1 1 5DC190D4
P 7150 3950
AR Path="/5D7E2920/5DC190D4" Ref="C?"  Part="1" 
AR Path="/5DAD577F/5DC190D4" Ref="C?"  Part="1" 
AR Path="/5DC190D4" Ref="C117"  Part="1" 
F 0 "C117" V 7050 3850 50  0000 C CNN
F 1 "C_opt" V 7050 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7150 3950 50  0001 C CNN
F 3 "~" H 7150 3950 50  0001 C CNN
	1    7150 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DE5A1EF
P 6650 3950
AR Path="/5D7E29BE/5DE5A1EF" Ref="C?"  Part="1" 
AR Path="/5DE5A1EF" Ref="C113"  Part="1" 
F 0 "C113" V 6550 4050 50  0000 C CNN
F 1 "C_opt" V 6550 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6650 3950 50  0001 C CNN
F 3 "~" H 6650 3950 50  0001 C CNN
	1    6650 3950
	1    0    0    1   
$EndComp
Text Notes 6250 3650 0    50   ~ 0
ADC_IN10\n
Wire Wire Line
	7150 3250 7150 3850
Connection ~ 7150 3250
Wire Wire Line
	6650 3850 6650 3050
Text Notes 6250 3450 0    50   ~ 0
DAC_OUT1
$Comp
L Device:C_Small C?
U 1 1 5DD268AF
P 6900 3950
AR Path="/5D7E29BE/5DD268AF" Ref="C?"  Part="1" 
AR Path="/5DD268AF" Ref="C115"  Part="1" 
F 0 "C115" V 6800 4050 50  0000 C CNN
F 1 "C_opt" V 6800 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6900 3950 50  0001 C CNN
F 3 "~" H 6900 3950 50  0001 C CNN
	1    6900 3950
	1    0    0    1   
$EndComp
Wire Wire Line
	6900 3850 6900 3150
Wire Wire Line
	6900 3150 7800 3150
Connection ~ 6900 3150
Wire Wire Line
	6650 3050 7800 3050
Connection ~ 6650 3050
Wire Wire Line
	7150 3250 7800 3250
Wire Notes Line
	8100 2900 8100 4300
Wire Notes Line
	8100 4300 6600 4300
Wire Notes Line
	6600 4300 6600 2900
Wire Notes Line
	6600 2900 8100 2900
Text Notes 6600 3000 0    50   ~ 0
Optional filtering for ADC
Wire Wire Line
	5750 4900 5750 4750
Wire Wire Line
	5850 1550 5850 1750
Wire Wire Line
	5750 1750 5750 1700
Wire Wire Line
	5650 1750 5650 1700
Wire Wire Line
	5550 1700 5550 1750
Wire Wire Line
	5450 1750 5450 1700
Wire Wire Line
	4800 2450 4950 2450
Wire Wire Line
	4950 2350 4800 2350
NoConn ~ 4950 2850
NoConn ~ 4950 2750
NoConn ~ 4950 2650
NoConn ~ 4950 3950
Wire Wire Line
	4400 3850 4950 3850
NoConn ~ 4950 3750
Wire Wire Line
	4500 3650 4950 3650
NoConn ~ 4950 3550
NoConn ~ 4950 3450
NoConn ~ 4950 3350
Wire Wire Line
	4450 3250 4950 3250
NoConn ~ 4950 4550
Wire Wire Line
	2800 4450 2950 4450
NoConn ~ 4950 4350
NoConn ~ 4950 4250
Wire Wire Line
	4450 4150 4950 4150
Wire Wire Line
	4450 4050 4950 4050
Wire Wire Line
	4950 3150 4450 3150
NoConn ~ 4950 3050
NoConn ~ 6250 3950
NoConn ~ 6250 3850
NoConn ~ 6250 3750
Wire Wire Line
	6250 3650 7700 3650
Wire Wire Line
	6250 3350 7400 3350
Wire Wire Line
	6250 3250 7150 3250
NoConn ~ 6250 4550
Wire Wire Line
	6250 4450 6350 4450
Wire Wire Line
	6250 4350 6350 4350
NoConn ~ 6250 4250
NoConn ~ 6250 4150
NoConn ~ 6250 4050
Wire Wire Line
	6250 3150 6900 3150
Wire Wire Line
	6250 3050 6650 3050
Wire Wire Line
	4050 1950 4950 1950
Wire Wire Line
	4900 2150 4950 2150
Wire Wire Line
	8850 3450 8850 2950
Wire Wire Line
	6250 3450 8850 3450
Text Label 8150 3450 0    50   ~ 0
load_dac
$Comp
L Device:L L103
U 1 1 5E407072
P 10050 1100
F 0 "L103" V 10240 1100 50  0000 C CNN
F 1 "100uH" V 10149 1100 50  0000 C CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 10050 1100 50  0001 C CNN
F 3 "~" H 10050 1100 50  0001 C CNN
	1    10050 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L104
U 1 1 5E40AD58
P 10050 1900
F 0 "L104" V 10240 1900 50  0000 C CNN
F 1 "100uH" V 10149 1900 50  0000 C CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 10050 1900 50  0001 C CNN
F 3 "~" H 10050 1900 50  0001 C CNN
	1    10050 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C124
U 1 1 5E40B030
P 9850 1450
F 0 "C124" H 10000 1450 50  0000 L CNN
F 1 "100nF" H 9900 1350 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9888 1300 50  0001 C CNN
F 3 "~" H 9850 1450 50  0001 C CNN
	1    9850 1450
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E40B548
P 9450 1450
AR Path="/5DF6D00C/5E40B548" Ref="C?"  Part="1" 
AR Path="/5E40B548" Ref="C123"  Part="1" 
F 0 "C123" H 9650 1450 50  0000 C CNN
F 1 "1uF" H 9650 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9450 1450 50  0001 C CNN
F 3 "~" H 9450 1450 50  0001 C CNN
	1    9450 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	9850 1350 9850 1100
Wire Wire Line
	9850 1100 9900 1100
Wire Wire Line
	9850 1900 9900 1900
Wire Wire Line
	9850 1550 9850 1900
Wire Wire Line
	9850 1100 9450 1100
Wire Wire Line
	9450 1100 9450 1350
Connection ~ 9850 1100
Wire Wire Line
	9450 1550 9450 1900
Wire Wire Line
	9450 1900 9850 1900
Connection ~ 9850 1900
Wire Wire Line
	10200 1100 10400 1100
Connection ~ 9450 1900
Connection ~ 9450 1100
Connection ~ 8500 1100
Connection ~ 8500 1900
Wire Wire Line
	10300 1900 10200 1900
$Comp
L power:GNDA #PWR0133
U 1 1 5E61D5B4
P 9450 1900
F 0 "#PWR0133" H 9450 1650 50  0001 C CNN
F 1 "GNDA" H 9450 1750 50  0000 C CNN
F 2 "" H 9450 1900 50  0001 C CNN
F 3 "" H 9450 1900 50  0001 C CNN
	1    9450 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 1100 8950 1100
Wire Wire Line
	8500 1900 9450 1900
$Comp
L power:+5VA #PWR0130
U 1 1 5E6A16F6
P 8950 900
F 0 "#PWR0130" H 8950 750 50  0001 C CNN
F 1 "+5VA" H 8965 1073 50  0000 C CNN
F 2 "" H 8950 900 50  0001 C CNN
F 3 "" H 8950 900 50  0001 C CNN
	1    8950 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 900  8950 1100
Connection ~ 8950 1100
Wire Wire Line
	8950 1100 9450 1100
$Comp
L power:+5VA #PWR0131
U 1 1 5E6E2575
P 9200 5250
F 0 "#PWR0131" H 9200 5100 50  0001 C CNN
F 1 "+5VA" H 9215 5423 50  0000 C CNN
F 2 "" H 9200 5250 50  0001 C CNN
F 3 "" H 9200 5250 50  0001 C CNN
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0132
U 1 1 5E6E2967
P 9200 6150
F 0 "#PWR0132" H 9200 5900 50  0001 C CNN
F 1 "GNDA" H 9200 6000 50  0000 C CNN
F 2 "" H 9200 6150 50  0001 C CNN
F 3 "" H 9200 6150 50  0001 C CNN
	1    9200 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0129
U 1 1 5E6E2EA2
P 8550 6250
F 0 "#PWR0129" H 8550 6000 50  0001 C CNN
F 1 "GNDA" H 8550 6100 50  0000 C CNN
F 2 "" H 8550 6250 50  0001 C CNN
F 3 "" H 8550 6250 50  0001 C CNN
	1    8550 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 6200 4350 6450
$Comp
L Device:C_Small C?
U 1 1 5E8A053C
P 4050 7100
AR Path="/5D7E29BE/5E8A053C" Ref="C?"  Part="1" 
AR Path="/5E8A053C" Ref="C106"  Part="1" 
F 0 "C106" V 3950 7150 50  0000 C CNN
F 1 "?" V 4150 7100 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4050 7100 50  0001 C CNN
F 3 "~" H 4050 7100 50  0001 C CNN
	1    4050 7100
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E8A0995
P 3850 6900
AR Path="/5D7E29BE/5E8A0995" Ref="R?"  Part="1" 
AR Path="/5E8A0995" Ref="R106"  Part="1" 
F 0 "R106" V 3950 6900 50  0000 C CNN
F 1 "?" V 3850 6900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3780 6900 50  0001 C CNN
F 3 "~" H 3850 6900 50  0001 C CNN
	1    3850 6900
	0    1    -1   0   
$EndComp
Wire Wire Line
	4000 6900 4050 6900
Wire Wire Line
	4050 6900 4050 7000
Wire Wire Line
	4350 6200 4200 6200
Connection ~ 4200 6200
$Comp
L Transistor_BJT:BC849 Q?
U 1 1 5E9A3251
P 4050 6700
AR Path="/5D7E29BE/5E9A3251" Ref="Q?"  Part="1" 
AR Path="/5E9A3251" Ref="Q102"  Part="1" 
F 0 "Q102" V 4050 6500 50  0000 L CNN
F 1 "BC849" V 3950 6400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 6625 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/Infineon-BC847SERIES_BC848SERIES_BC849SERIES_BC850SERIES-DS-v01_01-en.pdf?fileId=db3a304314dca389011541d4630a1657" H 4050 6700 50  0001 L CNN
	1    4050 6700
	0    -1   -1   0   
$EndComp
Connection ~ 4050 6900
Connection ~ 3400 6700
Wire Wire Line
	4250 6600 4350 6600
Connection ~ 4350 6600
Wire Wire Line
	4350 6600 4350 6750
Wire Wire Line
	3400 6450 3850 6450
Connection ~ 4350 6450
Wire Wire Line
	4350 6450 4350 6600
$Comp
L Jumper:SolderJumper_2_Bridged JP103
U 1 1 5EA87E44
P 4000 6450
F 0 "JP103" H 4000 6563 50  0000 C CNN
F 1 "vcc_filter" H 4000 6564 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 4000 6450 50  0001 C CNN
F 3 "~" H 4000 6450 50  0001 C CNN
	1    4000 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 6450 4350 6450
$Comp
L Jumper:SolderJumper_2_Open JP101
U 1 1 5EAA9FDA
P 3550 6900
F 0 "JP101" H 3550 7013 50  0000 C CNN
F 1 "vcc_filter" H 3550 7014 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3550 6900 50  0001 C CNN
F 3 "~" H 3550 6900 50  0001 C CNN
	1    3550 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6450 3400 6700
Wire Wire Line
	3700 6600 3700 6900
Wire Wire Line
	3700 6600 3850 6600
Wire Wire Line
	4050 7300 4350 7300
Wire Wire Line
	4050 7200 4050 7300
Connection ~ 4050 7300
Connection ~ 3400 7300
Wire Wire Line
	3400 7200 3400 7300
Wire Wire Line
	3400 7300 2950 7300
Wire Wire Line
	3400 7300 4050 7300
Connection ~ 3700 6900
Wire Notes Line
	4300 6400 4300 7400
Wire Notes Line
	4300 7400 3550 7400
Wire Notes Line
	3550 7400 3550 6400
Wire Notes Line
	3550 6400 4300 6400
Text Notes 3550 7400 0    50   ~ 0
Optional filter Vin\n
Wire Wire Line
	2800 950  2800 1500
NoConn ~ 6250 3550
Wire Wire Line
	5550 4750 5450 4750
Wire Wire Line
	5650 4750 5550 4750
Connection ~ 5550 4750
$Comp
L MCU_ST_STM32F3:STM32F301C8Tx U?
U 1 1 5DD2691F
P 5650 3250
AR Path="/5D7E29BE/5DD2691F" Ref="U?"  Part="1" 
AR Path="/5DD2691F" Ref="U103"  Part="1" 
F 0 "U103" H 5600 2500 50  0000 C CNN
F 1 "STM32F301C8Tx" H 5600 2600 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5050 1850 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00093332.pdf" H 5650 3250 50  0001 C CNN
	1    5650 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3750 2350 3750
Connection ~ 2300 3850
Connection ~ 2300 3950
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F0F2379
P 1200 3850
AR Path="/5D7E29BE/5F0F2379" Ref="J?"  Part="1" 
AR Path="/5F0F2379" Ref="J103"  Part="1" 
F 0 "J103" H 1350 3700 50  0000 C CNN
F 1 "24v" H 1350 3800 50  0000 C CNN
F 2 "michal_custom_edge:gpu_molex" H 1200 3850 50  0001 C CNN
F 3 "~" H 1200 3850 50  0001 C CNN
	1    1200 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 3950 1450 3850
Wire Wire Line
	1450 3850 1400 3850
$Comp
L power:+24V #PWR?
U 1 1 5F0F2387
P 1650 3750
AR Path="/5D7E29BE/5F0F2387" Ref="#PWR?"  Part="1" 
AR Path="/5F0F2387" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 1650 3600 50  0001 C CNN
F 1 "+24V" H 1665 3923 50  0000 C CNN
F 2 "" H 1650 3750 50  0001 C CNN
F 3 "" H 1650 3750 50  0001 C CNN
	1    1650 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 3750 1400 3750
Wire Wire Line
	1600 1000 1850 1000
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F113595
P 1400 3750
F 0 "#FLG0102" H 1400 3825 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 3900 50  0001 C CNN
F 2 "" H 1400 3750 50  0001 C CNN
F 3 "~" H 1400 3750 50  0001 C CNN
	1    1400 3750
	1    0    0    -1  
$EndComp
Connection ~ 1400 3750
$Comp
L power:GND #PWR0101
U 1 1 5E354127
P 600 900
F 0 "#PWR0101" H 600 650 50  0001 C CNN
F 1 "GND" H 605 727 50  0000 C CNN
F 2 "" H 600 900 50  0001 C CNN
F 3 "" H 600 900 50  0001 C CNN
	1    600  900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E354307
P 1850 1000
F 0 "#PWR0104" H 1850 750 50  0001 C CNN
F 1 "GND" H 1855 827 50  0000 C CNN
F 2 "" H 1850 1000 50  0001 C CNN
F 3 "" H 1850 1000 50  0001 C CNN
	1    1850 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E354ADF
P 1450 3950
F 0 "#PWR0105" H 1450 3700 50  0001 C CNN
F 1 "GND" H 1455 3777 50  0000 C CNN
F 2 "" H 1450 3950 50  0001 C CNN
F 3 "" H 1450 3950 50  0001 C CNN
	1    1450 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E35561E
P 1800 3050
F 0 "#PWR0106" H 1800 2800 50  0001 C CNN
F 1 "GND" V 1805 2922 50  0000 R CNN
F 2 "" H 1800 3050 50  0001 C CNN
F 3 "" H 1800 3050 50  0001 C CNN
	1    1800 3050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E35608B
P 2350 4900
F 0 "#PWR0107" H 2350 4650 50  0001 C CNN
F 1 "GND" H 2355 4727 50  0000 C CNN
F 2 "" H 2350 4900 50  0001 C CNN
F 3 "" H 2350 4900 50  0001 C CNN
	1    2350 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E356623
P 5450 4900
F 0 "#PWR0109" H 5450 4650 50  0001 C CNN
F 1 "GND" H 5455 4727 50  0000 C CNN
F 2 "" H 5450 4900 50  0001 C CNN
F 3 "" H 5450 4900 50  0001 C CNN
	1    5450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4900 5450 4750
Connection ~ 5450 4750
$Comp
L power:GND #PWR0114
U 1 1 5E3779DE
P 5400 7350
F 0 "#PWR0114" H 5400 7100 50  0001 C CNN
F 1 "GND" H 5405 7177 50  0000 C CNN
F 2 "" H 5400 7350 50  0001 C CNN
F 3 "" H 5400 7350 50  0001 C CNN
	1    5400 7350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E37805D
P 6850 6450
F 0 "#PWR0116" H 6850 6200 50  0001 C CNN
F 1 "GND" H 6855 6277 50  0000 C CNN
F 2 "" H 6850 6450 50  0001 C CNN
F 3 "" H 6850 6450 50  0001 C CNN
	1    6850 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 6050 6850 6450
$Comp
L power:GND #PWR0117
U 1 1 5E399581
P 10300 6300
F 0 "#PWR0117" H 10300 6050 50  0001 C CNN
F 1 "GND" H 10305 6127 50  0000 C CNN
F 2 "" H 10300 6300 50  0001 C CNN
F 3 "" H 10300 6300 50  0001 C CNN
	1    10300 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E399B03
P 10300 1900
F 0 "#PWR0119" H 10300 1650 50  0001 C CNN
F 1 "GND" H 10305 1727 50  0000 C CNN
F 2 "" H 10300 1900 50  0001 C CNN
F 3 "" H 10300 1900 50  0001 C CNN
	1    10300 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5E39BDAD
P 4350 2550
F 0 "#PWR0128" H 4350 2300 50  0001 C CNN
F 1 "GND" H 4355 2377 50  0000 C CNN
F 2 "" H 4350 2550 50  0001 C CNN
F 3 "" H 4350 2550 50  0001 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0134
U 1 1 5E39C2F1
P 2800 1550
F 0 "#PWR0134" H 2800 1300 50  0001 C CNN
F 1 "GND" H 2805 1377 50  0000 C CNN
F 2 "" H 2800 1550 50  0001 C CNN
F 3 "" H 2800 1550 50  0001 C CNN
	1    2800 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 5E3C4005
P 5650 6450
F 0 "#PWR0137" H 5650 6200 50  0001 C CNN
F 1 "GND" H 5655 6277 50  0000 C CNN
F 2 "" H 5650 6450 50  0001 C CNN
F 3 "" H 5650 6450 50  0001 C CNN
	1    5650 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 6250 10500 6250
$Comp
L Device:C_Small C?
U 1 1 5E4EBA2B
P 10900 5900
AR Path="/5D7E29BE/5E4EBA2B" Ref="C?"  Part="1" 
AR Path="/5E4EBA2B" Ref="C129"  Part="1" 
F 0 "C129" V 10850 5750 50  0000 C CNN
F 1 "100nF" V 10850 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10900 5900 50  0001 C CNN
F 3 "~" H 10900 5900 50  0001 C CNN
	1    10900 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 5800 10900 5500
Connection ~ 10900 5500
Wire Wire Line
	10900 5500 10700 5500
Wire Wire Line
	10900 6000 10900 6250
Connection ~ 10900 6250
Wire Wire Line
	10900 6250 10700 6250
$Comp
L Device:R R111
U 1 1 5E082FED
P 7950 3150
F 0 "R111" V 7900 2900 50  0000 C CNN
F 1 "R_opt" V 7950 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3150 50  0001 C CNN
F 3 "~" H 7950 3150 50  0001 C CNN
	1    7950 3150
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR0121
U 1 1 5E42AFB1
P 6650 4100
F 0 "#PWR0121" H 6650 3850 50  0001 C CNN
F 1 "GNDA" H 6650 3950 50  0000 C CNN
F 2 "" H 6650 4100 50  0001 C CNN
F 3 "" H 6650 4100 50  0001 C CNN
	1    6650 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0124
U 1 1 5E42B468
P 6900 4100
F 0 "#PWR0124" H 6900 3850 50  0001 C CNN
F 1 "GNDA" H 6900 3950 50  0000 C CNN
F 2 "" H 6900 4100 50  0001 C CNN
F 3 "" H 6900 4100 50  0001 C CNN
	1    6900 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0125
U 1 1 5E42B5CD
P 7150 4100
F 0 "#PWR0125" H 7150 3850 50  0001 C CNN
F 1 "GNDA" H 7150 3950 50  0000 C CNN
F 2 "" H 7150 4100 50  0001 C CNN
F 3 "" H 7150 4100 50  0001 C CNN
	1    7150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0126
U 1 1 5E42B86A
P 7400 4100
F 0 "#PWR0126" H 7400 3850 50  0001 C CNN
F 1 "GNDA" H 7400 3950 50  0000 C CNN
F 2 "" H 7400 4100 50  0001 C CNN
F 3 "" H 7400 4100 50  0001 C CNN
	1    7400 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0127
U 1 1 5E42BA3A
P 7700 4100
F 0 "#PWR0127" H 7700 3850 50  0001 C CNN
F 1 "GNDA" H 7700 3950 50  0000 C CNN
F 2 "" H 7700 4100 50  0001 C CNN
F 3 "" H 7700 4100 50  0001 C CNN
	1    7700 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R116
U 1 1 5E852AE6
P 3100 4450
F 0 "R116" V 3000 4400 50  0000 C CNN
F 1 "1k" V 3100 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 4450 50  0001 C CNN
F 3 "~" H 3100 4450 50  0001 C CNN
	1    3100 4450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 4450 4950 4450
$Comp
L Device:C C130
U 1 1 5E899F5A
P 2500 3700
F 0 "C130" H 2300 3700 50  0000 L CNN
F 1 "100nF" H 2200 3800 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2538 3550 50  0001 C CNN
F 3 "~" H 2500 3700 50  0001 C CNN
	1    2500 3700
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C131
U 1 1 5E89C408
P 2500 4100
F 0 "C131" H 2600 4100 50  0000 L CNN
F 1 "100nF" H 2550 4000 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2538 3950 50  0001 C CNN
F 3 "~" H 2500 4100 50  0001 C CNN
	1    2500 4100
	1    0    0    1   
$EndComp
Wire Wire Line
	2950 3550 2500 3550
Connection ~ 2350 3550
Wire Wire Line
	2350 3550 2350 3750
Connection ~ 2500 3550
Wire Wire Line
	2500 3550 2350 3550
Connection ~ 2500 3850
Wire Wire Line
	2500 3850 2300 3850
$Comp
L Transistor_FET:IPT015N10N5 Q101
U 1 1 5F3129DE
P 2450 4450
F 0 "Q101" H 2655 4496 50  0000 L CNN
F 1 "YJD15N10A" H 2655 4405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2650 4375 50  0001 L CIN
F 3 "http://www.infineon.com/dgdl/Infineon-IPT015N10N5-DS-v02_01-EN.pdf?fileId=5546d4624a75e5f1014ac94680661aff" H 2250 4750 50  0001 L CNN
	1    2450 4450
	-1   0    0    -1  
$EndComp
$Comp
L Device:D D103
U 1 1 5EC67C2A
P 2950 4100
F 0 "D103" V 2950 4300 50  0000 L CNN
F 1 "B260A-13-F" V 2900 4150 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 2950 4100 50  0001 C CNN
F 3 "~" H 2950 4100 50  0001 C CNN
	1    2950 4100
	0    1    1    0   
$EndComp
Connection ~ 2500 3950
Wire Wire Line
	2500 3950 2950 3950
Wire Wire Line
	2950 4250 2500 4250
Connection ~ 2500 4250
Wire Wire Line
	2500 4250 2350 4250
Connection ~ 2350 4250
Wire Wire Line
	2350 4050 2350 4250
Wire Wire Line
	900  1100 900  1200
Connection ~ 900  1100
Connection ~ 900  1200
Wire Wire Line
	900  1800 900  1900
Connection ~ 900  1800
Connection ~ 900  1900
$EndSCHEMATC
