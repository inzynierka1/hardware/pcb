EESchema Schematic File Version 4
LIBS:main_board-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 9550 2250 2    50   Input ~ 0
I2C2_SCL
Text GLabel 9550 2650 2    50   Input ~ 0
I2C2_SDA
$Comp
L power:-5V #PWR?
U 1 1 5DB2FD94
P 7900 3150
AR Path="/5D7E29BE/5DB2FD94" Ref="#PWR?"  Part="1" 
AR Path="/5DB2FD94" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7900 3250 50  0001 C CNN
F 1 "-5V" V 7915 3278 50  0000 L CNN
F 2 "" H 7900 3150 50  0001 C CNN
F 3 "" H 7900 3150 50  0001 C CNN
	1    7900 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DB2FD9A
P 8000 2250
AR Path="/5D7E29BE/5DB2FD9A" Ref="#PWR?"  Part="1" 
AR Path="/5DB2FD9A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8000 2000 50  0001 C CNN
F 1 "GND" V 8000 2000 50  0000 C CNN
F 2 "" H 8000 2250 50  0001 C CNN
F 3 "" H 8000 2250 50  0001 C CNN
	1    8000 2250
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DB2FDA6
P 9700 3650
F 0 "#PWR?" H 9700 3500 50  0001 C CNN
F 1 "+3.3V" V 9715 3778 50  0000 L CNN
F 2 "" H 9700 3650 50  0001 C CNN
F 3 "" H 9700 3650 50  0001 C CNN
	1    9700 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	9300 2150 9400 2150
Wire Wire Line
	9400 2150 9400 2250
Wire Wire Line
	9400 2450 9300 2450
Wire Wire Line
	9400 2250 9550 2250
Connection ~ 9400 2250
Wire Wire Line
	9400 2250 9400 2350
Wire Wire Line
	9300 2550 9400 2550
Wire Wire Line
	9400 2550 9400 2650
Wire Wire Line
	9400 2850 9300 2850
Wire Wire Line
	9550 2650 9400 2650
Connection ~ 9400 2650
Wire Wire Line
	9400 2650 9400 2750
Wire Wire Line
	9300 2950 9400 2950
Wire Wire Line
	9400 3750 9300 3750
$Comp
L power:GND #PWR?
U 1 1 5DB2FDBA
P 7900 3650
AR Path="/5D7E29BE/5DB2FDBA" Ref="#PWR?"  Part="1" 
AR Path="/5DB2FDBA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7900 3400 50  0001 C CNN
F 1 "GND" V 7900 3400 50  0000 C CNN
F 2 "" H 7900 3650 50  0001 C CNN
F 3 "" H 7900 3650 50  0001 C CNN
	1    7900 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 3450 8300 3450
Wire Wire Line
	8300 3550 8200 3550
Wire Wire Line
	8200 3550 8200 3650
Wire Wire Line
	9400 3350 9300 3350
Wire Wire Line
	9300 3550 9400 3550
Wire Wire Line
	9400 3550 9400 3650
Wire Wire Line
	9400 3950 9300 3950
Wire Wire Line
	9400 3750 9400 3850
Wire Wire Line
	8200 2850 8200 2750
Wire Wire Line
	8200 2150 8300 2150
Wire Wire Line
	8000 2250 8200 2250
Wire Wire Line
	8200 2250 8200 2150
Wire Wire Line
	8200 3950 8200 3850
Wire Wire Line
	8200 3950 8300 3950
Wire Wire Line
	8300 3050 8200 3050
Wire Wire Line
	9300 2250 9400 2250
Wire Wire Line
	9300 2350 9400 2350
Connection ~ 9400 2350
Wire Wire Line
	9400 2350 9400 2450
Wire Wire Line
	9300 2750 9400 2750
Connection ~ 9400 2750
Wire Wire Line
	9400 2750 9400 2850
Wire Wire Line
	9400 2650 9300 2650
Connection ~ 8200 2250
Wire Wire Line
	8300 2250 8200 2250
Wire Wire Line
	8300 2350 8200 2350
Wire Wire Line
	8200 2350 8200 2250
Wire Wire Line
	8300 2450 8200 2450
Connection ~ 8200 2350
Wire Wire Line
	8200 2450 8200 2350
Wire Wire Line
	8300 2550 8200 2550
Connection ~ 8200 2450
Wire Wire Line
	8200 2550 8200 2450
Wire Wire Line
	8300 2650 8200 2650
Connection ~ 8200 2550
Wire Wire Line
	8200 2650 8200 2550
Wire Wire Line
	8300 2750 8200 2750
Connection ~ 8200 2650
Connection ~ 8200 2750
Wire Wire Line
	8200 2750 8200 2650
Wire Wire Line
	9400 2950 9400 3050
Wire Wire Line
	9400 3050 9400 3150
Connection ~ 9400 3050
Wire Wire Line
	9300 3050 9400 3050
Wire Wire Line
	9300 3150 9400 3150
Connection ~ 9400 3150
Wire Wire Line
	9300 3850 9400 3850
Connection ~ 9400 3850
Wire Wire Line
	9400 3850 9400 3950
Wire Wire Line
	9300 3650 9400 3650
Connection ~ 9400 3750
Connection ~ 9400 3650
Wire Wire Line
	9400 3650 9400 3750
Connection ~ 8200 3650
Wire Wire Line
	8200 3650 8300 3650
Wire Wire Line
	8300 3750 8200 3750
Wire Wire Line
	8200 3750 8200 3650
Wire Wire Line
	8300 3850 8200 3850
Connection ~ 8200 3750
Connection ~ 8200 3850
Wire Wire Line
	8200 3850 8200 3750
Wire Wire Line
	8200 3050 8200 3150
Connection ~ 8200 3150
Wire Wire Line
	8300 3150 8200 3150
Wire Wire Line
	8300 3350 8200 3350
Connection ~ 8200 3350
Wire Wire Line
	8200 3350 8200 3450
$Comp
L power:+5V #PWR?
U 1 1 5DB2FE0C
P 9650 3050
AR Path="/5D7E29BE/5DB2FE0C" Ref="#PWR?"  Part="1" 
AR Path="/5DB2FE0C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9650 2900 50  0001 C CNN
F 1 "+5V" V 9650 3250 50  0000 C CNN
F 2 "" H 9650 3050 50  0001 C CNN
F 3 "" H 9650 3050 50  0001 C CNN
	1    9650 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 3150 8200 3150
Wire Wire Line
	9400 3050 9650 3050
Wire Wire Line
	7900 3650 8200 3650
Wire Wire Line
	9400 3650 9700 3650
$Comp
L nodemcu:NodeMCU_Amica_R2 U?
U 1 1 5DBC7FF2
P 3800 2900
F 0 "U?" H 3800 3825 50  0000 C CNN
F 1 "NodeMCU_Amica_R2" H 3800 3734 50  0000 C CNN
F 2 "nodemcu:NodeMCU_Amica_R2" V 3800 2950 50  0000 C CNN
F 3 "" H 4050 2900 50  0000 C CNN
	1    3800 2900
	1    0    0    -1  
$EndComp
$Comp
L main_board-rescue:10018784-10210TLF-10018784-10210TLF J?
U 1 1 5DAE19F0
P 8800 3050
F 0 "J?" H 8800 4217 50  0000 C CNN
F 1 "10018784-10210TLF" H 8800 4126 50  0000 C CNN
F 2 "AMPHENOL_10018784-10210TLF" H 8800 3050 50  0001 L BNN
F 3 "" H 8800 3050 50  0001 L BNN
F 4 "None" H 8800 3050 50  0001 L BNN "Pole4"
F 5 "Amphenol ICC" H 8800 3050 50  0001 L BNN "Pole5"
F 6 "None" H 8800 3050 50  0001 L BNN "Pole6"
F 7 "Conn PCI Express Card Edge SKT 36 POS 2mm Solder ST Thru-Hole Tray" H 8800 3050 50  0001 L BNN "Pole7"
F 8 "Unavailable" H 8800 3050 50  0001 L BNN "Pole8"
	1    8800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 2850 8200 2850
Wire Wire Line
	8300 2950 8200 2950
Wire Wire Line
	8200 2950 8200 3050
Connection ~ 8200 3050
Wire Wire Line
	8200 3150 8200 3350
Wire Wire Line
	9400 3150 9400 3350
Wire Wire Line
	9300 3450 9400 3450
Wire Wire Line
	9400 3450 9400 3350
Connection ~ 9400 3350
$EndSCHEMATC
